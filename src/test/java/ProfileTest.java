import com.carsharing.capstone.CarsharingApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class ProfileTest {
    @Autowired
    MockMvc mvc;


    @Test
    public void driverProfileTest() throws Exception {
        String apiEndpoint = "/profile/driverInfo/{id}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName").value("Dato"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName").value("Giorgishvili"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.totalNumberOfRides").value(1));
    }

    @Test
    public void givenDriverReviewsTest() throws Exception {
        String apiEndpoint = "/profile/driverReviews/{id}/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].reviewId").value(66));

    }

}
