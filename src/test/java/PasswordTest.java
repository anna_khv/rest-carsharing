import com.carsharing.capstone.CarsharingApplication;
import com.carsharing.capstone.dto.ChangePasswordDto;
import com.carsharing.capstone.dto.EmailDto;
import com.carsharing.capstone.dto.NewPasswordDto;
import com.carsharing.capstone.model.User;
import com.carsharing.capstone.service.IUserService;
import com.carsharing.capstone.validator.ValidatePasswordResetToken;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class PasswordTest {

    @Autowired
    MockMvc mvc;
    @MockBean
    ValidatePasswordResetToken resetTokenValidate;
    @MockBean
    IUserService userService;

    @Test
    public void getTokenTestForResetPassword() throws Exception {
        String endPoint = "/authorisation/sendTokenToMail";
        Mockito.when(userService.createTokenForUser(any(User.class))).thenReturn("asdff342");
        Mockito.doNothing().when(userService).createEmailAndSend(any(String.class), any(String.class));
        mvc.perform(MockMvcRequestBuilders.post(endPoint)
                        .content(asJsonString(new EmailDto("bachoBachan1222@gmail.com")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("email has been sent successfully"));
        Mockito.verify(userService).createTokenForUser(any(User.class));
        Mockito.verify(userService).createEmailAndSend(any(String.class), any(String.class));
    }

    @Test
    public void setNewPasswordTextGetRequest() throws Exception {
        String token = "zNuZnpDdPMYkqqpbqqRtEWjZdn";
        String endPoint = "/authorisation/setNewPassword";
        Mockito.when(resetTokenValidate.validateToken(Mockito.anyString())).thenReturn(new User());
        mvc.perform(MockMvcRequestBuilders.get(endPoint)
                        .param("token", token)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("token is valid"));
        Mockito.verify(resetTokenValidate).validateToken(Mockito.anyString());
    }


    @Test
    public void setNewPasswordTextPostRequest() throws Exception {
        String token = "zNuZnpDdPMYkqqpbqqRtEWjZd";
        String endPoint = "/authorisation/setNewPassword";
        Mockito.when(resetTokenValidate.validateToken(Mockito.anyString())).thenReturn(new User());
        mvc.perform(MockMvcRequestBuilders.post(endPoint)
                        .param("token", token)
                        .content(asJsonString(new NewPasswordDto("Funn$yCats500")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("password reset successfully"));
        Mockito.verify(resetTokenValidate).validateToken(Mockito.anyString());
    }

    @Test
    public void changePasswordTest() throws Exception {
        String endPoint = "/authorisation/changePassword";
        mvc.perform(MockMvcRequestBuilders.post(endPoint)
                        .content(asJsonString(new ChangePasswordDto("Bachan34", "PinPing11$", "PongPong23$")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("password changed successfully"));
    }

    @Test
    public void changePasswordWithWrongCurrendPasswordTest() throws Exception {
        String endPoint = "/authorisation/changePassword";
        mvc.perform(MockMvcRequestBuilders.post(endPoint)
                        .content(asJsonString(new ChangePasswordDto("Bachan34", "Ping11$", "PongPong23$")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("current password  not correct"));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
