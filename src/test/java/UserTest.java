import com.carsharing.capstone.CarsharingApplication;
import com.carsharing.capstone.dto.DriverDto;
import com.carsharing.capstone.dto.DriverRegisterDto;
import com.carsharing.capstone.dto.PassengerRegisterDto;
import com.carsharing.capstone.repository.UserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class UserTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    UserRepo userRepo;


    @Test
    public void testRegisterNewPassenger() throws Exception {
        String apiEndpoint = "/authorisation/passengerRegistration";
        mvc.perform(MockMvcRequestBuilders.post(apiEndpoint)
                        .content(asJsonString(new PassengerRegisterDto("Bela1997", "BelaKakabadze@gmail.com", "Bela", "Kakabadze", "598231123", "Wholetth$edogsout12")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("Successful Registration"));

    }

    @Test
    public void testRegisterPassengerWithRegisteredPassengerUsername() throws Exception {
        String apiEndpoint = "/authorisation/passengerRegistration";
        mvc.perform(MockMvcRequestBuilders.post(apiEndpoint)
                        .content(asJsonString(new PassengerRegisterDto("Bachan34", "BelaKakabadze@gmail.com", "Bela", "Kakabadze", "598231123", "Wholetth$edogsout12")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.error").value("Bad Request")); //need better message here
    }


    @Test
    public void testRegisterPassengerWithInvalidPasswordAndUsername() throws Exception {
        String apiEndpoint = "/authorisation/passengerRegistration";
        mvc.perform(MockMvcRequestBuilders.post(apiEndpoint)
                        .content(asJsonString(new PassengerRegisterDto("belabela", "BelaKakabadze@gmail.com", "Bela", "Kakabadze", "598231123", "wholetth$edogsout12")))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.password").value("password does not meet requirements"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.userName").value("username does not meet requirements"));

    }


    @Test
    public void testRegisterDriver() throws Exception {
        String apiEndpoint = "/authorisation/driverRegistration";
        mvc.perform(MockMvcRequestBuilders.post(apiEndpoint)
                        .content(asJsonString(new DriverRegisterDto(new PassengerRegisterDto("NikaNiko23", "NikaKipiani@gmail.com", "Nika", "Kipiani", "598231123", "Blue@Sky87"), new DriverDto("sdg3432", "2234213ed"))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("driver registered successfully"));
    }

    @Test
    public void testRegisterDriverInvalidCase() throws Exception {
        String apiEndpoint = "/authorisation/driverRegistration";
        mvc.perform(MockMvcRequestBuilders.post(apiEndpoint)
                        .content(asJsonString(new DriverRegisterDto(new PassengerRegisterDto("NIka99", "NikaKipiani@gmail.com", "Nika", "Kipiani", "598231123", "Blue@Sky87"), new DriverDto(null, ""))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$['driver.driverLicenseNumber']").value("driverLicenseNumber should be provided"))
                .andExpect(MockMvcResultMatchers.jsonPath("$['driver.carRegistrationNumber']").value("carRegistrationNumber should be provided"));
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}


