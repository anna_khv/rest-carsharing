import com.carsharing.capstone.CarsharingApplication;
import com.carsharing.capstone.dto.AddressData;
import com.carsharing.capstone.dto.AddressInfoDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RouteTest {
    @Autowired
    MockMvc mvc;

    @Test
    public void searchRouteText() throws Exception {
        String ApiEndpoint = "/home/searchRoute";
        mvc.perform(MockMvcRequestBuilders.post(ApiEndpoint)
                        .content(asJsonString(new AddressInfoDto(new AddressData("300 Aragveli", 3), List.of(), new AddressData("Kekelidze", 34))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));


    }

    @Test
    public void searchRoutewithseveralPassingPoint() throws Exception {
        String ApiEndpoint = "/home/searchRoute";
        mvc.perform(MockMvcRequestBuilders.post(ApiEndpoint)
                        .content(asJsonString(new AddressInfoDto(new AddressData("300 Aragveli", 3), List.of(new AddressData("Qetevan Tsamebuli", 2), new AddressData("Avlabari Metro", 0)), new AddressData("Kekelidze", 34))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].routePostId").value(509));
    }

    @Test
    public void getRoutePostsOnMainPageTest() throws Exception {
        String ApiEndpoint = "/home/posts/{page}";
        mvc.perform(MockMvcRequestBuilders.get(ApiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void getRoutePostsOnMainPageWithAuthenticatedPassengerTest() throws Exception {
        String ApiEndpoint = "/home/posts/{page}";
        mvc.perform(MockMvcRequestBuilders.get(ApiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].routePostId").value(500));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

