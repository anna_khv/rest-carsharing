import com.carsharing.capstone.CarsharingApplication;
import com.carsharing.capstone.dto.CreateReviewDto;
import com.carsharing.capstone.dto.EditReviewDto;
import com.carsharing.capstone.dto.FullNameDto;
import com.carsharing.capstone.model.Rate;
import com.carsharing.capstone.repository.UserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RideTest {

    @Autowired
    MockMvc mvc;
    @Autowired
    UserRepo userRepo;

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void aDriverGetPendingRidesTest() throws Exception {
        String apiEndpoint = "/ride/driver/getPendingRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].userInfos", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(55))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].userInfos", hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].rideId").value(59));

    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void bDriverApprovePassengerOnRideTest() throws Exception {
        String apiEndpoint = "/ride/driver/approvePassenger/{passengerId}/{rideId}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 2, 55)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("passenger is registered on given ride"));
    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void cDriverApprovePassengerOnRideWhichIsFinishedTest() throws Exception {
        String apiEndpoint = "/ride/driver/approvePassenger/{passengerId}/{rideId}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 3, 56)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("passenger is pending on already finished ride"));
    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void dDriverApprovePassengerOnRideWithNoAvailableSeatsTest() throws Exception {
        String apiEndpoint = "/ride/driver/approvePassenger/{passengerId}/{rideId}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 5, 59)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("there is no available seats on this ride to register this passenger"));
    }


    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void eDriverRejectPassengerOnRideTest() throws Exception {
        String apiEndpoint = "/ride/driver/rejectPassenger/{passengerId}/{rideId}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 5, 59)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("passenger is successfully rejected"));
    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void fPendingRidesOfPassengerTest() throws Exception {
        String apiEndpoint = "/ride/passenger/getPendingRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(59));

    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void gPassengerHistoryOfRidesTest() throws Exception {
        String apiEndpoint = "/ride/passenger/historyOfRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(56));
    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void hDriverHistoryOfRidesTest() throws Exception {
        String apiEndpoint = "/ride/driver/historyOfRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(56));
    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void iUpcomingRidesForPassengerTest() throws Exception {
        String apiEndpoint = "/ride/passenger/upcomingRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(55));

    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void jDriverUpcomingRidesTest() throws Exception {
        String apiEndpoint = "/ride/driver/upcomingRides/{page}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 1)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].rideId").value(55))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[1].rideId").value(59));
    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void kDriverCancelRideTest() throws Exception {
        String apiEndpoint = "/ride/driver/cancelRide/{rideId}";
        mvc.perform(MockMvcRequestBuilders.delete(apiEndpoint, 55)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("ride successfully canceled"));


    }


    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void lPassengerUnregisterFromRideTest() throws Exception {
        String apiEndpoint = "/ride/passenger/unregister/{rideId}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndpoint, 59)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("passenger unregistered successfully"));

    }


    @Test
    @WithMockUser(username = "MaMuKaMamu09", password = "MamukaMamu11", roles = {"PASSENGER"})
    public void mCreateReviewTest() throws Exception {

        String apiEndPoint = "/ride/review/createReview";
        mvc.perform(MockMvcRequestBuilders.post(apiEndPoint)
                        .content(asJsonString(new CreateReviewDto("calm and organized driver, would reccomend undoubtedly", Rate.FIVE, 56, new FullNameDto("Dato", "Giorgishvili"))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("review successfully created"));
    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void nEditReviewWithExpiredTimeTest() throws Exception {
        String apiEndPoint = "/ride/review/edit";
        mvc.perform(MockMvcRequestBuilders.put(apiEndPoint)
                        .content(asJsonString(new EditReviewDto(66, "bad driver", Rate.TWO)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("update time for this review is already expired"));
    }

    @Test
    @WithMockUser(username = "MaMuKaMamu09", password = "MamukaMamu11", roles = {"PASSENGER"})
    public void nEditReviewWithNotExpiredTimeTest() throws Exception {
        String apiEndPoint = "/ride/review/edit";
        mvc.perform(MockMvcRequestBuilders.put(apiEndPoint)
                        .content(asJsonString(new EditReviewDto(67, "bad bad bad", Rate.TWO)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("review successfully updated"));

    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void oDeleteReviewTest() throws Exception {
        String apiEndPoint = "/ride/review/del/{revId}";
        mvc.perform(MockMvcRequestBuilders.delete(apiEndPoint, 66)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("review successfully deleted"));
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
