import com.carsharing.capstone.CarsharingApplication;
import com.carsharing.capstone.dto.CreateAddressDto;
import com.carsharing.capstone.dto.CreatePostDto;
import com.carsharing.capstone.dto.CreateRideDto;
import com.carsharing.capstone.model.Address;
import com.carsharing.capstone.model.LocationPosition;
import com.carsharing.capstone.model.Route;
import com.carsharing.capstone.repository.AddressRepo;
import com.carsharing.capstone.repository.RouteRepo;
import com.carsharing.capstone.repository.UserRepo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class RoutePostTest {
    @Autowired
    MockMvc mvc;
    @Autowired
    UserRepo userRepo;
    @Autowired
    AddressRepo addressRepo;
    @Autowired
    RouteRepo routeRepo;

    @BeforeEach
    public void setup() throws Exception {
    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void passengerFollowRouteTest() throws Exception {
        String apiEndPoint = "/post/passenger/followRoute/{id}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndPoint, 33)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text")
                        .value("request is successful"));
    }

    @Test
    @WithMockUser(username = "invalidUser")
    public void passengerFollowRouteTestWithInvalidUser() throws Exception {
        String apiEndPoint = "/post/passenger/followRoute/{id}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndPoint, 33)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

    }

    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"PASSENGER"})
    public void passengerBookRideTest() throws Exception {
        String apiEndPoint = "/post/passenger/bookRide/{id}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndPoint, 55)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text")
                        .value("request has been sent successfully. Wait for the driver's response"));

    }

    @Test
    @WithMockUser(username = "invalidUser")
    public void passengerBookRideTestWithInvalidUser() throws Exception {
        String apiEndPoint = "/post/passenger/bookRide/{id}";
        mvc.perform(MockMvcRequestBuilders.get(apiEndPoint, 55)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void createRoutePostTest() throws Exception {
        String apiEndPoint = "/post/driver/createPost";
        Address ad1before = addressRepo.findByAdStreetAndPositionAndStreetNumber("Faliashvili", LocationPosition.START_POINT, 3)
                .orElse(new Address());
        Address ad2before = addressRepo.findByAdStreetAndPositionAndStreetNumber("Berbuki", LocationPosition.END_POINT, 23)
                .orElse(new Address());
        mvc.perform(MockMvcRequestBuilders.post(apiEndPoint)
                        .content(asJsonString(new CreatePostDto("offering a ride for 3 people in total", "i have a toyota camry 2008",
                                new CreateRideDto(new Date(2022 - 02 - 12), 3,
                                        Arrays.asList(new CreateAddressDto(LocationPosition.START_POINT, "Faliashvili", 3),
                                                new CreateAddressDto(LocationPosition.END_POINT, "Berbuki", 23))))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("routePost is successfully created"));
        Address ad1after = addressRepo.findByAdStreetAndPositionAndStreetNumber("Faliashvili", LocationPosition.START_POINT, 3)
                .orElse(new Address());
        Address ad2after = addressRepo.findByAdStreetAndPositionAndStreetNumber("Berbuki", LocationPosition.END_POINT, 23)
                .orElse(new Address());
        assertNotEquals(ad1before, ad1after);
        assertNotEquals(ad2before, ad2after);


    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void createRoutePostWithExistingRouteTest() throws Exception {
        Address ad1before = addressRepo.findByAdStreetAndPositionAndStreetNumber("300 Aragveli", LocationPosition.START_POINT, 4)
                .orElse(new Address());
        Address ad2before = addressRepo.findByAdStreetAndPositionAndStreetNumber("Bakhtrioni", LocationPosition.END_POINT, 24)
                .orElse(new Address());
        Route beforeRoute = routeRepo.findByAddressesAndSize(List.of(ad1before, ad2before), 2).orElse(new Route());

        String apiEndPoint = "/post/driver/createPost";
        mvc.perform(MockMvcRequestBuilders.post(apiEndPoint)
                        .content(asJsonString(new CreatePostDto("offering a ride for 3 people in total", "i have a toyota camry 2008",
                                new CreateRideDto(new Date(2022 - 02 - 12), 3,
                                        Arrays.asList(new CreateAddressDto(LocationPosition.START_POINT, "300 Aragveli", 4),
                                                new CreateAddressDto(LocationPosition.END_POINT, "Bakhtrioni", 24))))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("routePost is successfully created"));
        Address ad1after = addressRepo.findByAdStreetAndPositionAndStreetNumber("300 Aragveli", LocationPosition.START_POINT, 4)
                .orElse(new Address());
        Address ad2after = addressRepo.findByAdStreetAndPositionAndStreetNumber("Bakhtrioni", LocationPosition.END_POINT, 24)
                .orElse(new Address());
        Route afterRoute = routeRepo.findByAddressesAndSize(List.of(ad1after, ad2after), 2).orElse(new Route());
        assertEquals(ad1before, ad1after);
        assertEquals(ad2before, ad2after);
        assertEquals(beforeRoute.getId(), afterRoute.getId());

    }

    @Test
    @WithMockUser(username = "Datuna1992", password = "datunaDat12", roles = {"DRIVER"})
    public void createRoutePostWithOneExistingAddressTest() throws Exception {
        String apiEndPoint = "/post/driver/createPost";
        Address ad1before = addressRepo.findByAdStreetAndPositionAndStreetNumber("300 Aragveli", LocationPosition.START_POINT, 4)
                .orElse(new Address());
        Address ad2before = addressRepo.findByAdStreetAndPositionAndStreetNumber("Krwanisi", LocationPosition.END_POINT, 5)
                .orElse(new Address());
        mvc.perform(MockMvcRequestBuilders.post(apiEndPoint)
                        .content(asJsonString(new CreatePostDto("offering a ride for 3 people in total", "i have a toyota camry 2008",
                                new CreateRideDto(new Date(2022 - 02 - 12), 3,
                                        Arrays.asList(new CreateAddressDto(LocationPosition.START_POINT, "300 Aragveli", 4),
                                                new CreateAddressDto(LocationPosition.END_POINT, "Krwanisi", 5))))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("routePost is successfully created"));
        Address ad1after = addressRepo.findByAdStreetAndPositionAndStreetNumber("300 Aragveli", LocationPosition.START_POINT, 4)
                .orElse(new Address());
        Address ad2after = addressRepo.findByAdStreetAndPositionAndStreetNumber("Krwanisi", LocationPosition.END_POINT, 5)
                .orElse(new Address());
        assertEquals(ad1before, ad1after);
        assertNotEquals(ad2before, ad2after);

    }

    @Test
    @WithMockUser(username = "invalidUser")
    public void createRoutePostTestWithInvalidUser() throws Exception {
        String apiEndPoint = "/post/driver/createPost";
        mvc.perform(MockMvcRequestBuilders.post(apiEndPoint)
                        .content(asJsonString(new CreatePostDto("offering a ride for 3 people in total", "i have a toyota camry 2008",
                                new CreateRideDto(new Date(2022 - 02 - 12), 3,
                                        Arrays.asList(new CreateAddressDto(LocationPosition.START_POINT, "Faliashvili", 3),
                                                new CreateAddressDto(LocationPosition.END_POINT, "Berbuki", 23))))))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
