import com.carsharing.capstone.CarsharingApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application_integrationTest.properties")
@SpringBootTest(classes = {CarsharingApplication.class}, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
public class AdminTest {

    @Autowired
    MockMvc mvc;


    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"ADMIN"})
    public void deletePostTest() throws Exception {
        String apiEndpoint="/admin/post/{postId}";
        mvc.perform(MockMvcRequestBuilders.delete(apiEndpoint,509)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("routePost successfully deleted"));


    }
    @Test
    @WithMockUser(username = "Bachan34", password = "Ping11$", roles = {"ADMIN"})
    public void deleteFinishedPostTest() throws Exception {
        String apiEndpoint="/admin/post/{postId}";
        mvc.perform(MockMvcRequestBuilders.delete(apiEndpoint,501)
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError())
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("routePost can not be deleted because corresponding ride has already finished"));


    }

}
