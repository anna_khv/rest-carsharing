package com.carsharing.capstone.util;

public class MessageConstants {

    private MessageConstants() {
        throw new IllegalArgumentException("no constructor for static class");
    }

    public static final String DRIVER_NOT_AUTHENTICATED = "credentials do not belong to the driver";
    public static final String NOT_PASSENGER_CREDENTIALS = "credentials do not belong to the passenger";
    public static final String INCORRECT_CREDENTIALS = "credentials are not correct ";
    public static final String USERNAME_NOT_FOUND = "user with this username not found";
    public static final String RIDE_AND_DRIVER_MISMATCH = "the ride given in path parameter does not belong to the authenticated driver";
    public static final String EMAIL_NOT_FOUND = "user with this email not found";
    public static final String NOT_PENDING_ON_GIVEN_RIDE = "passenger is not pending on given ride";
    public static final String TOKEN_NOT_FOUND = "token was not found";
    public static final String TOKEN_EXPIRED = "token expired";
    public static final String PASSENGER_ID_NOT_FOUND = "passengerId can not be found";
    public static final String RIDE_ID_NOT_FOUND = " rideId can not be found";
    public static final String DRIVER_ID_NOT_FOUND = "Driver with given id is not found";
    public static final String REVIEW_ID_NOT_FOUND = "review with given id is not found";
    public static final String ROUTE_POST_WITH_GIVEN_ID_NOT_FOUND = "routePost with given id is not found";
    public static final String USER_NOT_ON_RIDE = "user is not registered on this ride";
    public static final String UNREGISTER_FROM_FINISHED_RIDE = "passenger tries to unregister from finished ride";
    public static final String CANCEL_FINISHED_RIDE = "driver can not cancel finished ride";
    public static final String REVIEW_NOT_OF_GIVEN_USER = "the author of this review is not given user";
    public static final String ROUTE_POST_FOR_FINISHED_RIDE = "routePost can not be deleted because corresponding ride has already finished";
    public static final String PENDING_ON_FINISHED_RIDE="passenger is pending on already finished ride";
}
