package com.carsharing.capstone.util;

public class Constants {
    Constants() {
        throw new IllegalArgumentException("no constructor for static class");
    }

    public static final int PAGE_SIZE = 10;
    public static final String PASSENGER = "PASSENGER";
    public static final String DRIVER = "DRIVER";
    public static final String ADMIN = "ADMIN";
}
