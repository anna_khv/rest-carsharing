package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User, Long> {
    Optional<User> findByUserName(String username);
    Optional<User> findByEmail(String email);
    Optional<User> findByToken(String token);
    List<User> findByFirstNameAndLastName(String firstName, String lastName);
}
