package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {

}
