package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface DriverRepo extends JpaRepository<Driver, Long> {
    @Modifying
    @Transactional
    @Query(value = "INSERT INTO Driver (user_id,car_reg_num,driver_license) VALUES (?1, ?2, ?3)", nativeQuery = true)
    void saveWithParentId(long parentId, String regNumber, String LicenseNumber);
    Optional<Driver> findByUserName(String username);
}
