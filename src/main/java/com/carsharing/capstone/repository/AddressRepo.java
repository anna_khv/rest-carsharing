package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;

import com.carsharing.capstone.model.Address;
import com.carsharing.capstone.model.LocationPosition;

import java.util.List;
import java.util.Optional;


public interface AddressRepo extends JpaRepository<Address, Long> {

    List<Address> findByAdStreetContainingAndPosition(String address, LocationPosition pos);

   Optional<Address> findByAdStreetAndPositionAndStreetNumber(String address, LocationPosition pos, int streetNumber);


}
