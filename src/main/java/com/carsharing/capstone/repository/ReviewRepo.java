package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ReviewRepo extends JpaRepository<Review,Long> {

    @Query("SELECT rev FROM Review rev JOIN rev.ride r ON r.rideId =?1 " +
            " JOIN rev.addresseeUser toUser ON toUser.idNumber=?2" +
            " JOIN rev.user author ON author.idNumber=?3" )
    Optional<Review> findByRideIdAndAddresseeUserIdAndUserId(long ride, long addresseeUser, long author);

}
