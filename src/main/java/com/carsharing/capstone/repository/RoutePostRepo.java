package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Ride;

import com.carsharing.capstone.model.Driver;
import com.carsharing.capstone.model.RoutePost;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface RoutePostRepo extends JpaRepository<RoutePost, Long> {

    List<RoutePost> findByRide(Ride rid);
    List<RoutePost> findByDriver(Driver driver);

    @Query(value="SELECT * FROM Route_Post  ORDER BY RAND()", nativeQuery = true)
    List<RoutePost> findRoutePosts(Pageable pageable);
}
