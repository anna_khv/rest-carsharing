package com.carsharing.capstone.repository;

import com.carsharing.capstone.model.Address;
import com.carsharing.capstone.model.Route;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface RouteRepo extends JpaRepository<Route, Long> {


    @Query("SELECT  r FROM Route r JOIN r.containsAddresses a ON a.id=?1"
            + " LEFT  JOIN r.containsAddresses b ON b.id=?2"
            + " LEFT JOIN r.containsAddresses c ON c.id=?3")
    List<Route> findByAddressId(long startId, long passId, long endId);

    @Query("SELECT r FROM Route r JOIN r.containsAddresses a GROUP BY r HAVING SUM(CASE WHEN a  IN ?1 THEN 1 ELSE -1 END)=?2")
    Optional<Route> findByAddressesAndSize(List<Address> addresses, long size);

    @Query("SELECT r FROM Route r JOIN r.containsAddresses a ON a IN ?1 GROUP BY r HAVING COUNT(a)=?2")
    List<Route> findByAddressIn(List<Address> addresses, long size);


}
