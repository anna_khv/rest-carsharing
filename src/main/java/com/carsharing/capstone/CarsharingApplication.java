package com.carsharing.capstone;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
@Slf4j
public class CarsharingApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarsharingApplication.class, args);
		log.info("Application Running");
	}

}
