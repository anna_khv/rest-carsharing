package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.MessageDto;
import com.carsharing.capstone.service.IPostService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;

@RestController
@RequestMapping("admin")
@Slf4j
public class AdminController {
    @Autowired
    IPostService postService;


    @ApiOperation("returns message whether routePost is successfully deleted ")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "routePost successfully deleted")
    @DeleteMapping("post/{postId}")
    public ResponseEntity<MessageDto> deletePost(@PathVariable long postId) {
        postService.deletePost(postId);
        MessageDto mes = new MessageDto();
        mes.setText("routePost successfully deleted");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }


}
