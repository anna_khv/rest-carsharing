package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.AddressInfoDto;
import com.carsharing.capstone.dto.PostDto;
import com.carsharing.capstone.service.IRouteService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.HttpURLConnection;
import java.util.List;


@RestController
@RequestMapping("home")
@Slf4j
public class RouteController {

    @Autowired
    IRouteService service;

    @ApiOperation("returns list of routePosts which correspond with searched addresses")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of routePosts"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "empty list")})
    @PostMapping("/searchRoute")
    public ResponseEntity<List<PostDto>> searchWithRoute(@RequestBody @Valid AddressInfoDto data) {

        List<PostDto> results = service.getSearchedRoutePosts(data);
        if (!results.isEmpty()) {
            return new ResponseEntity<>(results, HttpStatus.OK);
        } else {
            log.debug("no content");
            return new ResponseEntity<>(results, HttpStatus.NO_CONTENT);
        }
    }

    @ApiOperation("returns list of routePosts according to routes which specific passenger follows or random" +
            "routePosts if passenger does not follow any routes")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of routePost"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "empty list")})
    @GetMapping("/posts/{page}")
    public ResponseEntity<List<PostDto>> getPostsOnMainPage(@PathVariable int page) {
        List<PostDto> result = service.getPostsFromFollowedRoutesOrRandomly(page);
        if (!result.isEmpty()) {
            return new ResponseEntity<>(result, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(result, HttpStatus.NO_CONTENT);
        }

    }

}
