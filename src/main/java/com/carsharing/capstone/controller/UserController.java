package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.*;
import com.carsharing.capstone.exception.AuthenticationExceptionForDriverOrPassenger;
import com.carsharing.capstone.exception.TokenNotFoundException;
import com.carsharing.capstone.model.Driver;
import com.carsharing.capstone.model.User;
import com.carsharing.capstone.repository.PasswordResetTokenRepo;
import com.carsharing.capstone.repository.UserRepo;
import com.carsharing.capstone.service.IUserService;
import com.carsharing.capstone.util.MessageConstants;
import com.carsharing.capstone.validator.ValidatePasswordResetToken;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.net.HttpURLConnection;

@RestController
@RequestMapping("/authorisation")
@Slf4j
public class UserController {
    @Autowired
    IUserService userService;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    UserRepo userRepo;
    @Autowired
    PasswordEncoder encoder;
    @Autowired
    PasswordResetTokenRepo passwordResetTokenRepo;
    @Autowired
    ValidatePasswordResetToken validatePasswordResetToken;

    @ApiOperation(value = "returns message whether user is registered or not(if not error message is sent)")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "user is registered successfully", response = MessageDto.class),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "user data is not valid", response = MessageDto.class)
    })
    @PostMapping("/passengerRegistration")
    public ResponseEntity<MessageDto> passengerRegistration(@RequestBody @Valid PassengerRegisterDto userdto) {
        MessageDto message = new MessageDto();
        User user = modelMapper.typeMap(PassengerRegisterDto.class, User.class)
                .map(userdto);
        User userRegistered = userService.createPassengerUser(user);
        log.info("passenger {} registered successfully", userRegistered.getUserName());
        message.setText("Successful Registration");
        return new ResponseEntity<>(message, HttpStatus.CREATED);
    }

    @ApiOperation(value = "returns message whether driver is registered or not(if not error message is sent)")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "driver is registered successfully", response = MessageDto.class),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "driver data is not valid", response = MessageDto.class)
    })
    @PostMapping("/driverRegistration")
    public ResponseEntity<MessageDto> driverRegistration(@RequestBody @Valid DriverRegisterDto driverdtoData) {
        MessageDto message = new MessageDto();
        User user = modelMapper.map(driverdtoData.getUser(), User.class);
        Driver driver = modelMapper.typeMap(DriverDto.class, Driver.class)
                .addMappings(maper -> maper.skip(Driver::setDriver))
                .map(driverdtoData.getDriver());
        userService.createDriverUser(user, driver);
        message.setText("driver registered successfully");
        log.info("driver {} registered successfully", user.getUserName());
        return new ResponseEntity<>(message, HttpStatus.CREATED);


    }

    @PostMapping("/sendTokenToMail")
    @ApiOperation(value = "returns message whether token has been sent to given email successfully")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "email has been sent successfully")
    public ResponseEntity<MessageDto> sendTokenToEmail(HttpServletRequest request, @RequestBody @Valid EmailDto email) throws TokenNotFoundException {
        User user = userRepo.findByEmail(email.getEmail()).orElseThrow(() -> {
            log.debug(MessageConstants.EMAIL_NOT_FOUND);
            throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.EMAIL_NOT_FOUND);
        });
        String token = userService.createTokenForUser(user);
        String host = "http://localhost:8080";
        String link = host + String.format("/authorisation/setNewPassword?token=%s", token);
        userService.createEmailAndSend(link, email.getEmail());
        MessageDto message = new MessageDto();
        message.setText("email has been sent successfully");
        return new ResponseEntity<>(message, HttpStatus.OK);

    }

    @PostMapping("/setNewPassword")
    @ApiOperation(value = "returns message whether password is reset successfully")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "password reset successfully")
    public ResponseEntity<MessageDto> setNewPassword(@RequestBody @Valid NewPasswordDto password, @RequestParam(name = "token") String param) {
        User user = validatePasswordResetToken.validateToken(param);
        String encoded = encoder.encode(password.getResetPassword());
        MessageDto message = new MessageDto();
        user.setPassword(encoded);
        userRepo.save(user);
        message.setText("password reset successfully");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @GetMapping("/setNewPassword")
    @ApiOperation(value = "returns message whether token is valid")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "token is valid")
    public ResponseEntity<MessageDto> setNewPassword(@RequestParam(name = "token") String param) {
        validatePasswordResetToken.validateToken(param);
        MessageDto message = new MessageDto();
        message.setText("token is valid");
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

    @PostMapping("/changePassword")
    @ApiOperation("returns message whether password is successfully changed")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "password changed successfully"),
            @ApiResponse(code = HttpURLConnection.HTTP_UNAUTHORIZED, message = "current password  not correct")})
    public ResponseEntity<MessageDto> changePassword(@RequestBody @Valid ChangePasswordDto data) {
        log.debug("changing password");
        MessageDto message = new MessageDto();
        User user = userRepo.findByUserName(data.getUserName()).orElseThrow(
                () -> {
                    log.debug(MessageConstants.USERNAME_NOT_FOUND);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.USERNAME_NOT_FOUND);
                }
        );
        boolean match = BCrypt.checkpw(data.getOldPassword(), user.getPassword());
        if (match) {
            String encodedPassword = encoder.encode(data.getNewPassword());
            user.setPassword(encodedPassword);
            userRepo.save(user);
            message.setText("password changed successfully");
            return new ResponseEntity<>(message, HttpStatus.OK);
        } else {
            message.setText("current password  not correct");
            return new ResponseEntity<>(message, HttpStatus.UNAUTHORIZED);
        }


    }


    @GetMapping("/logout")
    public void logout() {
        // this method should appear in swagger ui
    }

    @PostMapping("/login")
    public void login(@RequestBody LoginDto data) {
        // this method should appear in swagger ui
    }


}
