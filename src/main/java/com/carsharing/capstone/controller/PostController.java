package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.CreatePostDto;
import com.carsharing.capstone.dto.MessageDto;
import com.carsharing.capstone.dto.PostDto;
import com.carsharing.capstone.service.IPostService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.HttpURLConnection;


@RestController
@RequestMapping("post")
@Slf4j
public class PostController {
    @Autowired
    IPostService postService;

    @GetMapping("/passenger/followRoute/{id}")
    @ApiOperation("returns message whether passenger successfully followed chosen route")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "request is successful")
    public ResponseEntity<MessageDto> followRoute(@PathVariable long id) {
        postService.followRoute(id);
        MessageDto message = new MessageDto();
        message.setText("request is successful");
        return new ResponseEntity<>(message, HttpStatus.OK);

    }

    @GetMapping("/passenger/bookRide/{id}")
    @ApiOperation("returns message whether passenger successfully sent ride request  to driver")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "request has been sent successfully. Wait for the driver's response")
    public ResponseEntity<MessageDto> bookARide(@PathVariable long id) {
        postService.bookARide(id);
        MessageDto message = new MessageDto();
        message.setText("request has been sent successfully. Wait for the driver's response");
        return new ResponseEntity<>(message, HttpStatus.OK);

    }

    @PostMapping("/driver/createPost")
    @ApiOperation("returns message whether routePost is created successfully")
    @ApiResponse(code = HttpURLConnection.HTTP_CREATED, message = "routePost is successfully created")
    public ResponseEntity<MessageDto> createARide(@RequestBody @Valid CreatePostDto post) {
        log.debug("entered in to createpost controller");
        long postId=postService.createRoutePost(post);
        MessageDto message = new MessageDto();
        message.setText("routePost is successfully created");
        return new ResponseEntity<>(message, HttpStatus.CREATED);

    }
}
