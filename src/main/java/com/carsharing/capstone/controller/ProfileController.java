package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.DriverProfileDto;
import com.carsharing.capstone.dto.ReviewDto;
import com.carsharing.capstone.service.IUserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.HttpURLConnection;
import java.util.List;

@RestController
@RequestMapping("/profile")
@Slf4j
public class ProfileController {
    @Autowired
    IUserService userService;

    @GetMapping("/driverInfo/{id}")
    @ApiOperation("returns driver profile")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "contains driver profile info", response = DriverProfileDto.class)
    public ResponseEntity<DriverProfileDto> driverProfile(@PathVariable Long id) {
        DriverProfileDto prof = userService.driverProfile(id);
        return new ResponseEntity<>(prof, HttpStatus.OK);

    }

    @GetMapping("/driverReviews/{id}/{page}")
    @ApiOperation("returns reviews for given drivers")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "contains driver profile info", response = List.class),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "contains driver profile info", response = List.class)
    })
    public ResponseEntity<List<ReviewDto>> driverReviews(@PathVariable Long id, @PathVariable int page) {
        List<ReviewDto> rev = userService.reviewsToDriver(id, page);
        if (!rev.isEmpty()) {
            return new ResponseEntity<>(rev, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(rev, HttpStatus.NO_CONTENT);
        }
    }
}
