package com.carsharing.capstone.controller;

import com.carsharing.capstone.dto.*;
import com.carsharing.capstone.service.IReviewService;
import com.carsharing.capstone.service.IRideService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.HttpURLConnection;
import java.util.List;

@RestController
@RequestMapping("ride")
@Slf4j
public class RideController {
    @Autowired
    IRideService rideService;
    @Autowired
    IReviewService reviewService;

    @GetMapping("/driver/getPendingRides/{page}")
    @ApiOperation("returns list with users who are waiting for drivers approval to register on ride")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "pending users list"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "empty list when no users are pending")})
    public ResponseEntity<List<UserOnRideDto>> getPendingUsersOnRidesForThisDriver(@PathVariable int page) {  //for drivers
        List<UserOnRideDto> pendingUsersOnRides = rideService.getPendingUsersOnRidesForThisDriver(page);
        if (!pendingUsersOnRides.isEmpty()) {
            return new ResponseEntity<>(pendingUsersOnRides, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(pendingUsersOnRides, HttpStatus.NO_CONTENT);
        }


    }

    @ApiOperation("returns message whether passenger got registered on given ride by driver")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "passenger is registered on given ride"),
            @ApiResponse(code = HttpURLConnection.HTTP_BAD_REQUEST, message = "there is no available seats on this ride to register this passenger ")})
    @GetMapping("/driver/approvePassenger/{passengerId}/{rideId}")
    public ResponseEntity<MessageDto> approvePassengerOnPendingRide(@PathVariable Long passengerId, @PathVariable Long rideId) {  //for drivers
        boolean result = rideService.approvePassengerOnPendingRide(passengerId, rideId);
        MessageDto mes = new MessageDto();
        if (result) {
            mes.setText("passenger is registered on given ride");
            return new ResponseEntity<>(mes, HttpStatus.OK);
        } else {
            mes.setText("there is no available seats on this ride to register this passenger");
            log.debug(mes.getText());
            return new ResponseEntity<>(mes, HttpStatus.BAD_REQUEST);
        }


    }

    @ApiOperation("returns message whether pending passenger has been successfully rejected ")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "passenger is successfully rejected")
    @GetMapping("/driver/rejectPassenger/{passengerId}/{rideId}")
    public ResponseEntity<MessageDto> rejectPassengerOnPendingRide(@PathVariable Long passengerId, @PathVariable Long rideId) {  //for drivers
        rideService.rejectPassengerOnPendingRide(passengerId, rideId);
        MessageDto mes = new MessageDto();
        mes.setText("passenger is successfully rejected");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }

    @ApiOperation("returns lists about rides for which passenger is pending")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of rides on which passenger is pending"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "passenger is not pending on any ride")})
    @GetMapping("/passenger/getPendingRides/{page}")
    public ResponseEntity<List<RideDto>> pendingRidesForPassenger(@PathVariable int page) { //for passengers
        List<RideDto> rides = rideService.pendingRidesForPassenger(page);
        if (!rides.isEmpty()) {
            return new ResponseEntity<>(rides, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(rides, HttpStatus.NO_CONTENT);
        }

    }

    @ApiOperation("returns list of past rides for passenger")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of past rides for passenger"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "no past rides for this passenger")})
    @GetMapping("/passenger/historyOfRides/{page}")
    public ResponseEntity<List<PassengerRideDto>> historyOfPastRidesForPassenger(@PathVariable int page) {  //for passenger
        List<PassengerRideDto> rides = rideService.historyOfPastRidesForPassenger(page);
        if (!rides.isEmpty()) {
            return new ResponseEntity<>(rides, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(rides, HttpStatus.NO_CONTENT);
        }

    }

    @ApiOperation("returns list of past rides for drivers")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of past rides for drivers"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "not past rides for this driver")})
    @GetMapping("/driver/historyOfRides/{page}")
    public ResponseEntity<List<DriverRideDto>> historyOfPastRidesForDriver(@PathVariable int page) {   //for driver
        List<DriverRideDto> rides = rideService.historyOfPastRidesForDriver(page);
        if (!rides.isEmpty()) {
            return new ResponseEntity<>(rides, HttpStatus.OK);
        } else {
            log.debug("history is empty for this driver");
            return new ResponseEntity<>(rides, HttpStatus.NO_CONTENT);
        }
    }

    @ApiOperation("returns lists of upcoming rides for passenger")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of upcoming rides on which passenger is registered"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "no upcoming rides for passenger")})
    @GetMapping("/passenger/upcomingRides/{page}")
    public ResponseEntity<List<PassengerRideDto>> upcomingRidesForPassenger(@PathVariable int page) {  //for passenger
        List<PassengerRideDto> rides = rideService.upcomingRidesForPassenger(page);
        if (!rides.isEmpty()) {
            return new ResponseEntity<>(rides, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(rides, HttpStatus.NO_CONTENT);
        }
    }

    @ApiOperation("returns lists of upcoming rides for drivers")
    @ApiResponses(value = {@ApiResponse(code = HttpURLConnection.HTTP_OK, message = "list of upcoming rides for driver"),
            @ApiResponse(code = HttpURLConnection.HTTP_NO_CONTENT, message = "no upcoming rides for driver")})
    @GetMapping("/driver/upcomingRides/{page}")
    public ResponseEntity<List<DriverRideDto>> upcomingRidesForDriver(@PathVariable int page) {  //for driver
        List<DriverRideDto> rides = rideService.upcomingRidesForDriver(page);
        if (!rides.isEmpty()) {
            return new ResponseEntity<>(rides, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(rides, HttpStatus.NO_CONTENT);
        }
    }


    @ApiOperation("returns message whether driver successfully canceled ride ")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "ride successfully canceled")
    @DeleteMapping("driver/cancelRide/{rideId}")
    public ResponseEntity<MessageDto> cancelRide(@PathVariable long rideId) {
        rideService.cancelRideByDriver(rideId);
        MessageDto mes = new MessageDto();
        mes.setText("ride successfully canceled");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }

    @ApiOperation("returns message whether passenger is successfully unregistered from ride ")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "passenger unregistered successfully")
    @GetMapping("/passenger/unregister/{rideId}")
    public ResponseEntity<MessageDto> unregisterFromRide(@PathVariable long rideId) {
        rideService.unregisterFromRideByPassenger(rideId);
        MessageDto mes = new MessageDto();
        mes.setText("passenger unregistered successfully");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);


    }

    @ApiOperation("returns message whether review has been created")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "review successfully created")
    @PostMapping("/review/createReview")
    public ResponseEntity<MessageDto> createReview(@RequestBody CreateReviewDto rev) {
        log.debug("creating review");
        reviewService.createReview(rev);
        MessageDto mes = new MessageDto();
        mes.setText("review successfully created");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }

    @ApiOperation("returns message whether review has been updated")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "review successfully updated")
    @PutMapping("/review/edit")
    public ResponseEntity<MessageDto> editReview(@RequestBody EditReviewDto rev) {
        reviewService.editReview(rev);
        MessageDto mes = new MessageDto();
        mes.setText("review successfully updated");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }


    @ApiOperation("returns message whether review has been deleted")
    @ApiResponse(code = HttpURLConnection.HTTP_OK, message = "review successfully deleted")
    @DeleteMapping("/review/del/{revId}")
    public ResponseEntity<MessageDto> deleteReview(@PathVariable long revId) {
        reviewService.deleteReview(revId);
        MessageDto mes = new MessageDto();
        mes.setText("review successfully deleted");
        log.debug(mes.getText());
        return new ResponseEntity<>(mes, HttpStatus.OK);
    }

}
