package com.carsharing.capstone.validator;

import com.carsharing.capstone.exception.TokenExpiredException;
import com.carsharing.capstone.exception.TokenNotFoundException;
import com.carsharing.capstone.model.PasswordResetToken;
import com.carsharing.capstone.model.User;
import com.carsharing.capstone.repository.PasswordResetTokenRepo;
import com.carsharing.capstone.util.MessageConstants;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;


@NoArgsConstructor
@Slf4j
public class ValidatePasswordResetToken {
    @Autowired
    PasswordResetTokenRepo passwordResetTokenRepo;


    public User validateToken(String token) {
        PasswordResetToken resetToken = passwordResetTokenRepo.findByToken(token).orElseThrow(() -> {
            log.debug(MessageConstants.TOKEN_NOT_FOUND);
            throw new TokenNotFoundException(MessageConstants.TOKEN_NOT_FOUND);
        });
        LocalDateTime time = LocalDateTime.now();
        if (time.isAfter(resetToken.getExpiryDate())) {
            log.debug(MessageConstants.TOKEN_EXPIRED);
            throw new TokenExpiredException(MessageConstants.TOKEN_EXPIRED);
        }
        return resetToken.getUser();
    }


}
