package com.carsharing.capstone.security;

import com.carsharing.capstone.model.User;
import com.carsharing.capstone.repository.UserRepo;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Slf4j
public class MyUserDetailService implements UserDetailsService {
    @Autowired
    private UserRepo userRepo;

    @Override

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<User> user = userRepo.findByUserName(username);
        if (user.isEmpty()) {
            log.info("no user found with this credentials");
            throw new UsernameNotFoundException("user " + username + " not found");
        } else {
            log.info("user found with this credentials");
            return new UserPrincipal(user.get());
        }
    }
}
