package com.carsharing.capstone.security;

import com.carsharing.capstone.dto.LoginDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@Slf4j
public class CustomAuthFilter extends UsernamePasswordAuthenticationFilter {


    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        log.debug("entered in customised filter");
        HttpServletRequest req = (HttpServletRequest) request;
        LoginDto user;
        CustomRequestWrapper customRequestWrapper = new CustomRequestWrapper(req);
        if ("POST".equalsIgnoreCase(req.getMethod()) && req.getRequestURL().toString().endsWith("/authorisation/login")) {
            log.info(req.getRequestURL().toString());
            try {
                String answer = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
                customRequestWrapper.setPayload(answer);
                ObjectMapper mapper = new ObjectMapper();
                user = mapper.readValue(answer, LoginDto.class);
                customRequestWrapper.addParam("username", user.getUsername());
                customRequestWrapper.addParam("password", user.getPassword());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String answer = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        customRequestWrapper.setPayload(answer);
        chain.doFilter(customRequestWrapper, response);


    }


}
