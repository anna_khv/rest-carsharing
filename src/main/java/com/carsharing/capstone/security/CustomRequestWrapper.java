package com.carsharing.capstone.security;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.ByteArrayInputStream;
import java.util.HashMap;

public class CustomRequestWrapper extends HttpServletRequestWrapper {
    private final HashMap<String, String>  params;
    private String payload;

    /**
     * Constructs a request object wrapping the given request.
     *
     * @param request The request to wrap
     * @throws IllegalArgumentException if the request is null
     */
    public CustomRequestWrapper(HttpServletRequest request) {
        super(request);
        params = new HashMap<>();

    }

    @Override
    public String getParameter(String name) {
        if (super.getParameter(name) != null) return super.getParameter(name);
        if (params.containsKey(name)) {
            return params.get(name);
        }

        return null;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public ServletInputStream getInputStream() {
        final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(payload.getBytes());
        return new ServletInputStreamImp(byteArrayInputStream);

    }

    public void addParam(String k, String v) {
        params.put(k, v);
    }

    public String getParams() {
        return params.toString();
    }
}
