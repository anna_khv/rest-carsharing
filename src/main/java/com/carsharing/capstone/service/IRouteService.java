package com.carsharing.capstone.service;

import com.carsharing.capstone.dto.AddressInfoDto;
import com.carsharing.capstone.dto.PostDto;

import java.util.List;

public interface IRouteService {

    List<PostDto> getSearchedRoutePosts(AddressInfoDto data);

    List<PostDto> getPostsFromFollowedRoutesOrRandomly(int page);

}
