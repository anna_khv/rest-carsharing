package com.carsharing.capstone.service;

import com.carsharing.capstone.dto.CreateReviewDto;
import com.carsharing.capstone.dto.EditReviewDto;

public interface IReviewService {
    void createReview(CreateReviewDto review);

    void editReview(EditReviewDto review);

    void deleteReview(long id);
}
