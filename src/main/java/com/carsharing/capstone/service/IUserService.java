package com.carsharing.capstone.service;

import com.carsharing.capstone.dto.DriverProfileDto;
import com.carsharing.capstone.dto.ReviewDto;
import com.carsharing.capstone.model.Driver;
import com.carsharing.capstone.model.User;

import java.util.List;

public interface IUserService {

    User createPassengerUser(User user);

    void createDriverUser(User user, Driver userDriver);

    DriverProfileDto driverProfile(Long id);

    List<ReviewDto> reviewsToDriver(Long id, int page);

    String createTokenForUser(User user);

    void createEmailAndSend(String link, String emailAddress);
}
