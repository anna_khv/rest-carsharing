package com.carsharing.capstone.service;

import com.carsharing.capstone.dto.DriverRideDto;
import com.carsharing.capstone.dto.PassengerRideDto;
import com.carsharing.capstone.dto.RideDto;
import com.carsharing.capstone.dto.UserOnRideDto;

import java.util.List;

public interface IRideService {

    List<UserOnRideDto> getPendingUsersOnRidesForThisDriver(int page);

    List<RideDto> pendingRidesForPassenger(int page);

    Boolean approvePassengerOnPendingRide(Long passengerId, Long rideId);

    void rejectPassengerOnPendingRide(Long passengerId, Long rideId);

    List<PassengerRideDto> historyOfPastRidesForPassenger(int page);

    List<PassengerRideDto> upcomingRidesForPassenger(int page);

    List<DriverRideDto> historyOfPastRidesForDriver(int page);

    List<DriverRideDto> upcomingRidesForDriver(int page);

    void cancelRideByDriver(long rideId);

    void  unregisterFromRideByPassenger(long rideId);
}
