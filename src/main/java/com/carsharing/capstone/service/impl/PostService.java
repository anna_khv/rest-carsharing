package com.carsharing.capstone.service.impl;

import com.carsharing.capstone.dto.CreateAddressDto;
import com.carsharing.capstone.dto.CreatePostDto;
import com.carsharing.capstone.exception.AuthenticationExceptionForDriverOrPassenger;
import com.carsharing.capstone.exception.FinishedRideException;
import com.carsharing.capstone.exception.IdNotFoundException;
import com.carsharing.capstone.model.*;
import com.carsharing.capstone.repository.*;
import com.carsharing.capstone.service.IPostService;
import com.carsharing.capstone.util.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class PostService implements IPostService {
    @Autowired
    RideRepo rideRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    RouteRepo routeRepo;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    RoutePostRepo routePostRepo;
    @Autowired
    DriverRepo driverRepo;
    @Autowired
    AddressRepo addressRepo;

    @Override
    public void bookARide(Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String name = principal.getUsername();
        User user = userRepo.findByUserName(name).orElseThrow(() -> {
            log.warn(MessageConstants.USERNAME_NOT_FOUND);
            throw new UsernameNotFoundException(MessageConstants.USERNAME_NOT_FOUND);
        });
        Optional<Ride> ride = rideRepo.findById(id);
        Ride rride = ride.orElseThrow(() -> {
            log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
        });
        rride.getPendingUsers().add(user);
        rideRepo.save(rride);
    }


    @Override
    public void followRoute(Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String name = principal.getUsername();
        User user = userRepo.findByUserName(name).orElseThrow(() -> {
            log.warn(MessageConstants.USERNAME_NOT_FOUND);
            throw new UsernameNotFoundException(MessageConstants.USERNAME_NOT_FOUND);
        });
        Optional<Route> route = routeRepo.findById(id);
        Route rroute = route.orElseThrow(() -> {
            log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
        });

        if (user.getFollowsRoutes().contains(rroute)) {
            user.getFollowsRoutes().remove(rroute);  //if user already follows this route , use will
            // unfollow this route
        } else {
            user.getFollowsRoutes().add(rroute);

        }
        userRepo.save(user);

    }


    @Override
    public Long createRoutePost(CreatePostDto routePost) {  //for driver
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username).orElseThrow(() -> {
            log.warn(MessageConstants.INCORRECT_CREDENTIALS);
            throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.INCORRECT_CREDENTIALS);
        });
        RoutePost post = modelMapper.typeMap(CreatePostDto.class, RoutePost.class)
                //  .addMappings(mapper -> mapper.<List<Address>>map(src -> src.getRide().getRoute(), (des, v) -> des.getRide().getRoute().setContainsAddresses(v)))
                .map(routePost);

        List<Address> addresses = routePost.getRide().getRoute().stream()
                .map(ad ->
                        addressRepo.findByAdStreetAndPositionAndStreetNumber(ad.getAdStreet(), ad.getPosition(), ad.getStreetNumber())
                                .orElseGet(() -> {
                                    Address result = modelMapper.typeMap(CreateAddressDto.class, Address.class).map(ad);
                                    return addressRepo.save(result);
                                })).toList();
        log.debug("addresses " + addresses);
        Route resultRoute = routeRepo.findByAddressesAndSize(addresses, addresses.size()).orElseGet(() -> {
            Route route = new Route();
            route.setContainsAddresses(addresses);
            return routeRepo.save(route);
        });

        log.debug("found route " + resultRoute);
        post.getRide().setRoute(resultRoute);
        post.setAvailableSeatsLeft(post.getRide().getTotalNumberOfPassengers());
        post.setDriver(driver);
        post.getRide().setPost(post);
        log.debug("created post {}", post);
       return  routePostRepo.save(post).getRoutePostId();
    }

    @Override
    public void deletePost(long postId) {
        RoutePost post = routePostRepo.findById(postId).orElseThrow(() -> {
                    log.warn(MessageConstants.ROUTE_POST_WITH_GIVEN_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.ROUTE_POST_WITH_GIVEN_ID_NOT_FOUND);
                }
        );
        if (post.getRide().isFinished()) {
            log.warn(MessageConstants.ROUTE_POST_FOR_FINISHED_RIDE);
            throw new FinishedRideException(MessageConstants.ROUTE_POST_FOR_FINISHED_RIDE);
        }
        routePostRepo.deleteById(postId);
    }

    @Override
    public int calculateRideCost(Ride ride) {
        return 0;
    }
}
