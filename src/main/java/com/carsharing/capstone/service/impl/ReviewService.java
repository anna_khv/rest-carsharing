package com.carsharing.capstone.service.impl;

import com.carsharing.capstone.dto.CreateReviewDto;
import com.carsharing.capstone.dto.EditReviewDto;
import com.carsharing.capstone.dto.FullNameDto;
import com.carsharing.capstone.exception.*;
import com.carsharing.capstone.model.Review;
import com.carsharing.capstone.model.Ride;
import com.carsharing.capstone.model.User;
import com.carsharing.capstone.repository.ReviewRepo;
import com.carsharing.capstone.repository.RideRepo;
import com.carsharing.capstone.repository.UserRepo;
import com.carsharing.capstone.service.IReviewService;
import com.carsharing.capstone.util.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class ReviewService implements IReviewService {
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    RideRepo rideRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    ReviewRepo reviewRepo;

    @Override
    public void createReview(CreateReviewDto review) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String name = principal.getUsername();
        User author = userRepo.findByUserName(name).orElseThrow(() -> {
            log.warn(MessageConstants.USERNAME_NOT_FOUND);
            throw new UsernameNotFoundException(MessageConstants.USERNAME_NOT_FOUND);
        });

        Converter<Long, Ride> convertToRide = (src) -> {
            return rideRepo.findById(src.getSource()).orElseThrow(() -> {
                        log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
                        throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
                    }
            );
        };
        Converter<FullNameDto, User> fullNameToUser = (src) -> {
            FullNameDto fullName = src.getSource();
            List<User> users = userRepo.findByFirstNameAndLastName(fullName.getFirstName(), fullName.getLastName());
            return users.stream().filter(user -> {
                Ride ride = rideRepo.findById(review.getRideId()).orElseThrow(() -> {
                    log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
                });
                return ride.getRegisteredUsers().contains(user) && ride.isFinished() || ride.getPost().getDriver().equals(user);
            }).findAny().orElseThrow(() -> {
                log.warn(MessageConstants.USER_NOT_ON_RIDE);
                throw new UserNotOnGivenRideException(MessageConstants.USER_NOT_ON_RIDE);
            });
        };

        Review result = modelMapper.typeMap(CreateReviewDto.class, Review.class)
                .addMappings(mapper -> mapper.using(convertToRide).map(CreateReviewDto::getRideId, Review::setRide))
                .addMappings(mapper -> mapper.using(fullNameToUser).map(CreateReviewDto::getAddresseeFullName, Review::setAddresseeUser))
                .map(review);
        result.setUser(author);
        result.setExpiryTime(LocalDateTime.now().plusMinutes(Review.EXPIRATION));
        reviewRepo.findByRideIdAndAddresseeUserIdAndUserId(result.getRide().getRideId(), result.getAddresseeUser().getIdNumber(), author.getIdNumber())
                .ifPresent((foundRev) -> {
                    log.warn("rev already exist " + foundRev);
                    throw new ReviewAlreadyPresentForThisRideAndAddresseeException("review already exist " + foundRev);
                });
        log.debug("review object is {}", result);
        reviewRepo.save(result);

    }

    @Override
    public void editReview(EditReviewDto review) {
        Review foundReview = reviewRepo.findById(review.getReviewId()).orElseThrow(() -> {
            log.warn("review does not exist to update");
            throw new IdNotFoundException("review does not exist to update");
        });
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String name = principal.getUsername();
        User author = userRepo.findByUserName(name).orElseThrow(() -> {
            log.warn(MessageConstants.USERNAME_NOT_FOUND);
            throw new UsernameNotFoundException(MessageConstants.USERNAME_NOT_FOUND);
        });
        if (!author.getReviewsOfUser().contains(foundReview)) {
            log.debug(MessageConstants.REVIEW_NOT_OF_GIVEN_USER);
            throw new ReviewNotByGivenUserException(MessageConstants.REVIEW_NOT_OF_GIVEN_USER);
        }

        LocalDateTime time = foundReview.getExpiryTime();
        LocalDateTime now = LocalDateTime.now();
        if (time.isAfter(now)) {
            foundReview.setRate(review.getRate());
            foundReview.setText(review.getText());
            reviewRepo.save(foundReview);

        } else {
            log.warn("update time for this review is already expired");
            throw new ReviewUpdateExpiredException("update time for this review is already expired");
        }
    }


    @Override
    public void deleteReview(long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String name = principal.getUsername();
        User author = userRepo.findByUserName(name).orElseThrow(() -> {
            log.warn(MessageConstants.USERNAME_NOT_FOUND);
            throw new UsernameNotFoundException(MessageConstants.USERNAME_NOT_FOUND);
        });
        User authorUser = reviewRepo.findById(id).orElseThrow(() -> {
            log.warn(MessageConstants.REVIEW_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.REVIEW_ID_NOT_FOUND);
        }).getUser();
        if (authorUser.equals(author)) {
            reviewRepo.deleteById(id);
        } else {
            log.warn(MessageConstants.REVIEW_NOT_OF_GIVEN_USER);
            throw new ReviewNotByGivenUserException(MessageConstants.REVIEW_NOT_OF_GIVEN_USER);
        }


    }
}
