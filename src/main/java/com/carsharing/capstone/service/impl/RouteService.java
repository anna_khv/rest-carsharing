package com.carsharing.capstone.service.impl;

import com.carsharing.capstone.dto.AddressData;
import com.carsharing.capstone.dto.AddressDto;
import com.carsharing.capstone.dto.AddressInfoDto;
import com.carsharing.capstone.dto.PostDto;
import com.carsharing.capstone.model.*;
import com.carsharing.capstone.repository.*;
import com.carsharing.capstone.service.IRouteService;
import com.carsharing.capstone.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@Service
@Slf4j
public class RouteService implements IRouteService {
    @Autowired
    AddressRepo addressrepo;
    @Autowired
    RouteRepo routeRepo;
    @Autowired
    RideRepo rideRepo;
    @Autowired
    RoutePostRepo routePostRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    ModelMapper modelMapper;


    @Override
    public List<PostDto> getSearchedRoutePosts(AddressInfoDto data) {
        AddressData startData = data.getAddressAtStartLocation();
        List<AddressData> passData = data.getAddressAtPassingLocation();
        AddressData endData = data.getAddressAtEndLocation();
        List<List<Address>> allSearchedAddresses = new ArrayList<>();
        List<Address> ad1 = addressrepo.findByAdStreetContainingAndPosition(startData.getStreet(), LocationPosition.START_POINT);
        List<Address> ad2 = addressrepo.findByAdStreetContainingAndPosition(endData.getStreet(), LocationPosition.END_POINT);
        allSearchedAddresses.add(ad1);
        allSearchedAddresses.add(ad2);
        passData.forEach(addressData -> {
            allSearchedAddresses.add(addressrepo.findByAdStreetContainingAndPosition(addressData.getStreet(), LocationPosition.PASSING_POINT));
        });
        List<List<Address>> allcombos = helper(allSearchedAddresses);
        log.debug("all combos {}", allcombos);
        Set<Route> allRoutes = allcombos.stream().map(list -> {
            return routeRepo.findByAddressIn(list, list.size());
        }).flatMap(Collection::stream).collect(Collectors.toSet());
        log.debug("get all routes {}", allRoutes);
        List<Ride> rides = allRoutes.stream().
                filter(Objects::nonNull)
                .map(route -> rideRepo.findByRouteId(route.getId()))
                .flatMap(Collection::stream)
                .toList();
        LocalDate dateNow= LocalDate.now();
        return rides.stream()
                .filter(ride->!ride.isFinished())
                .filter(ride->ride.getDate().toLocalDate().isAfter(dateNow))
                .map(Ride::getPost)
                .map(this::mapPostToPostDto).filter(Objects::nonNull)
                .toList();
    }


    private List<List<Address>> helper(List<List<Address>> addresses) {
        List<List<Address>> allData = new ArrayList<>();
        addresses.forEach(addList -> {
            if (!addList.isEmpty()) {
                allData.add(addList);
            }
        });

        if (allData.size() > 1) {//if we have more than one list than we need all permutations of each list elements
            List<List<Address>> result = new ArrayList<>();
            List<Address> present = new ArrayList<>();
            RouteService.log.debug("list contains one or more lists , so all combination of list elements should be calculated");
            cartesianProduct(allData, result, 0, present);
            return result;
        } else {// this validation shall be done in advance
            RouteService.log.debug("only addresses with starting location point or passing location point or destination location point is found {}", allData);
            return allData.stream().flatMap(Collection::stream).map(Arrays::asList).toList();
        }

    }


    private void cartesianProduct(List<List<Address>> given, List<List<Address>> modified, int depth, List<Address> present) {
        if (depth == given.size()) {
            List<Address> list = new ArrayList<>(present);
            modified.add(list);
            return;
        }

        for (int i = 0; i < given.get(depth).size(); i++) {
            present.add(given.get(depth).get(i));
            depth++;
            cartesianProduct(given, modified, depth, present);
            depth--;
            present.remove(given.get(depth).get(i));

        }

    }

    @Override
    public List<PostDto> getPostsFromFollowedRoutesOrRandomly(int page) {
        //when user is not authenticated the posts on main page should be random
        //when user is authenticated but follows no routes posts shall be still random
        //when user follows routes , posts shall be taken from posts that contain this route
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal().equals("anonymousUser")) {

            return getRandomPosts(page);
        } else {
            UserDetails principal = (UserDetails) authentication.getPrincipal();
            String username = principal.getUsername();
            Optional<User> user = userRepo.findByUserName(username);
            if (user.isPresent()) {
                User userr = user.get();
                List<Route> routes = userr.getFollowsRoutes();
                if (routes.size() >= 1) {
                    int size = Constants.PAGE_SIZE;
                    int start = page > 1 ? page * size : 0;
                    int end = start + size - 1;
                    return routes.stream()
                            .map(Route::getInRides)
                            .flatMap(Collection::stream)
                            .filter(ride -> !ride.isFinished())
                            .skip(start)
                            .limit(end)
                            .map(Ride::getPost)
                            .map(this::mapPostToPostDto).filter(Objects::nonNull)
                            .toList();
                } else {
                    return getRandomPosts(page);
                }
            } else {
                return getRandomPosts(page);
            }
        }


    }


    private List<PostDto> getRandomPosts(int page) {
        RouteService.log.debug("user is not authorised or follows any routes or on routes that he " +
                "follows there is no driverposts, therefore random posts should be selected for home page");
        int size = Constants.PAGE_SIZE;
        Pageable pageable = PageRequest.of(page - 1, size);
        LocalDate date=LocalDate.now();
        return routePostRepo.findRoutePosts(pageable).stream()
                .map(this::mapPostToPostDto)
                .filter(Objects::nonNull)
                .filter(post->post.getRide().getRideDate().toLocalDate().isAfter(date))
                .toList();

    }


    private PostDto mapPostToPostDto(RoutePost post) {
        if (post == null) return null;
        Converter<Driver, Long> convertToTotalNumberofRides = val -> val.getSource().getDriveRoutePosts()
                .stream()
                .map(RoutePost::getRide)
                .map(Ride::isFinished)
                .count();
        Converter<Driver, Long> convertToTotalNumberOfReviews = val -> (long) val.getSource().getReviewsToUser().size();
        Converter<Driver, Double> convertToAveregeRating = val -> val.getSource().getReviewsToUser().stream()
                .map(rev -> rev.getRate().getValue()).mapToLong(Long::longValue)
                .average().orElse(0.0);

        return modelMapper.typeMap(RoutePost.class, PostDto.class)
                .addMappings(mapper -> mapper.<List<AddressDto>>map(src -> src.getRide().getRoute().getContainsAddresses(), (dest, v) -> dest.getRide().setRoute(v)))
                .addMapping(RoutePost::getDriver, PostDto::setDriverProfile)
                .addMappings(mapper -> mapper.using(convertToTotalNumberofRides).<Long>map(RoutePost::getDriver, (des, val) -> des.getDriverProfile().setTotalNumberOfRides(val)))
                .addMappings(mapper -> mapper.using(convertToTotalNumberOfReviews).<Long>map(RoutePost::getDriver, (des, val) -> des.getDriverProfile().setTotalNumberOfReviews(val)))
                .addMappings(mapper -> mapper.using(convertToAveregeRating).<Double>map(RoutePost::getDriver, (des, val) -> des.getDriverProfile().setAvgRating(val)))
                .map(post);

    }

}
