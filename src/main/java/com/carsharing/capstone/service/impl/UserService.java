package com.carsharing.capstone.service.impl;

import com.carsharing.capstone.config.EmailConfig;
import com.carsharing.capstone.dto.DriverProfileDto;
import com.carsharing.capstone.dto.ReviewDto;
import com.carsharing.capstone.exception.IdNotFoundException;
import com.carsharing.capstone.model.*;
import com.carsharing.capstone.repository.DriverRepo;
import com.carsharing.capstone.repository.PasswordResetTokenRepo;
import com.carsharing.capstone.repository.UserRepo;
import com.carsharing.capstone.service.IUserService;
import com.carsharing.capstone.util.Constants;
import com.carsharing.capstone.util.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

@Service
@Slf4j
public class UserService implements IUserService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    DriverRepo driverRepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PasswordResetTokenRepo passwordResetTokenRepo;
    @Autowired
    EmailConfig emailConfig;

    @Override
    public User createPassengerUser(User user) {
        log.debug("user is {}", user);
        String encodedPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);
        return userRepo.save(user);

    }

    @Override
    public void createDriverUser(User user, Driver userDriver) {

        String encodedPass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encodedPass);
        User savedUser = createPassengerUser(user);
        long id = savedUser.getIdNumber();
        driverRepo.saveWithParentId(id, userDriver.getCarRegistrationNumber(), userDriver.getDriverLicenseNumber());
    }

    @Override
    public DriverProfileDto driverProfile(Long id) {
        Optional<Driver> userr = driverRepo.findById(id);
        Driver user = userr.orElseThrow(() -> {
            log.warn(MessageConstants.DRIVER_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.DRIVER_ID_NOT_FOUND);
        });
        Converter<Driver, Long> convertToTotalNumberofRides = val -> val.getSource().getDriveRoutePosts()
                .stream()
                .map(RoutePost::getRide)
                .filter(Ride::isFinished)
                .count();
        Converter<Driver, Long> convertToTotalNumberOfReviews = val -> (long) val.getSource().getReviewsToUser().size();
        Converter<Driver, Double> convertToAveregeRating = val -> val.getSource().getReviewsToUser().stream()
                .map(rev -> rev.getRate().getValue())
                .mapToLong(Long::longValue).average().orElse(0.0);
        return modelMapper.typeMap(Driver.class, DriverProfileDto.class)
                .addMappings(mapper -> mapper.using(convertToTotalNumberOfReviews).map(driver -> driver, DriverProfileDto::setTotalNumberOfReviews))
                .addMappings(mapper -> mapper.using(convertToTotalNumberofRides).map(driver -> driver, DriverProfileDto::setTotalNumberOfRides))
                .addMappings(mapper -> mapper.using(convertToAveregeRating).map(driver -> driver, DriverProfileDto::setAvgRating))
                .map(user);
    }


    @Override
    public List<ReviewDto> reviewsToDriver(Long id, int page) {
        Optional<Driver> driver = driverRepo.findById(id);
        Driver driverr = driver.orElseThrow(() -> {
            log.warn(MessageConstants.DRIVER_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.DRIVER_ID_NOT_FOUND);
        });
        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        List<Review> reviews = driverr.getReviewsToUser().stream().skip(start).limit(size).toList();
        return reviews.stream()
                .map(rev -> modelMapper.typeMap(Review.class, ReviewDto.class)
                        .addMapping(Review::getAddresseeUser, ReviewDto::setAddressee)
                        .addMapping(Review::getUser, ReviewDto::setAuthor)
                        .map(rev))
                .toList();
    }


    @Override
    public String createTokenForUser(User user) {
        log.debug("token creating");
        PasswordResetToken token = new PasswordResetToken();
        token.setToken(RandomStringUtils.randomAlphanumeric(80));
        token.setUser(user);
        LocalDateTime time = LocalDateTime.now().plusMinutes(PasswordResetToken.EXPIRATION);
        token.setExpiryDate(time);
        passwordResetTokenRepo.save(token);
        return token.getToken();
    }


    @Override
    public void createEmailAndSend(String link, String emailAddress) {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(emailConfig.getHost());
        mailSender.setPort(emailConfig.getPort());
        mailSender.setUsername(emailConfig.getUsername());
        mailSender.setPassword(emailConfig.getPassword());
        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(emailAddress);
        simpleMailMessage.setFrom("annakhvedelidze@gmail.com");
        simpleMailMessage.setSubject("resetPassword");
        String text = "please follow this link to reset your password" + "\n" + link + "\n\n" +
                "best regards" + "\n" + "carsharing team";

        simpleMailMessage.setText(text);
        mailSender.send(simpleMailMessage);

    }
}
