package com.carsharing.capstone.service.impl;

import com.carsharing.capstone.dto.DriverRideDto;
import com.carsharing.capstone.dto.PassengerRideDto;
import com.carsharing.capstone.dto.RideDto;
import com.carsharing.capstone.dto.UserOnRideDto;
import com.carsharing.capstone.exception.*;
import com.carsharing.capstone.model.Driver;
import com.carsharing.capstone.model.Ride;
import com.carsharing.capstone.model.RoutePost;
import com.carsharing.capstone.model.User;
import com.carsharing.capstone.repository.DriverRepo;
import com.carsharing.capstone.repository.RideRepo;
import com.carsharing.capstone.repository.RoutePostRepo;
import com.carsharing.capstone.repository.UserRepo;
import com.carsharing.capstone.service.IRideService;
import com.carsharing.capstone.util.Constants;
import com.carsharing.capstone.util.MessageConstants;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class RideService implements IRideService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    RoutePostRepo routePostRepo;
    @Autowired
    DriverRepo driverRepo;
    @Autowired
    RideRepo rideRepo;
    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<UserOnRideDto> getPendingUsersOnRidesForThisDriver(int page) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                });

        List<RoutePost> postsOfThisDriver = routePostRepo.findByDriver(driver);
        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        return postsOfThisDriver.stream()
                .map(RoutePost::getRide)
                .filter(ride -> !ride.isFinished())
                .map(ride ->
                        modelMapper.typeMap(Ride.class, UserOnRideDto.class)
                                .addMapping(Ride::getPendingUsers, UserOnRideDto::setUserInfos)
                                .map(ride))
                .skip(start)
                .limit(size)
                .toList();

    }


    @Override
    public List<RideDto> pendingRidesForPassenger(int page) { //for passengers
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Optional<User> passenger = userRepo.findByUserName(username);
        User user = passenger.orElseThrow(() -> {
            log.warn(MessageConstants.INCORRECT_CREDENTIALS);
            throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.INCORRECT_CREDENTIALS);
        });
        if (user.isDriver()) {
            log.warn(MessageConstants.NOT_PASSENGER_CREDENTIALS);
            throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.NOT_PASSENGER_CREDENTIALS);
        } else {
            int size = Constants.PAGE_SIZE;
            int start = page > 1 ? page * size : 0;
            return user.getPending().stream()
                    .filter(ride -> !ride.isFinished())
                    .map(ride -> modelMapper.typeMap(Ride.class, RideDto.class)
                            .addMapping(Ride::getDate, RideDto::setRideDate)
                            .addMapping(ride1 -> ride1.getRoute().getContainsAddresses(), RideDto::setRoute)
                            .map(ride))
                    .skip(start)
                    .limit(size)
                    .toList();

        }
    }

    @Override
    public Boolean approvePassengerOnPendingRide(Long passengerId, Long rideId) {  //for drivers
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                });
        User passenger = userRepo.findById(passengerId)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.PASSENGER_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.PASSENGER_ID_NOT_FOUND);
                });
        Ride ride = rideRepo.findById(rideId)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
                });
        if (ride.isFinished()) {
            log.debug(MessageConstants.PENDING_ON_FINISHED_RIDE);
            throw new FinishedRideException(MessageConstants.PENDING_ON_FINISHED_RIDE);
        }
        if (ride.getPost().getDriver().equals(driver)) {
            if (ride.getPendingUsers().contains(passenger)) {
                ride.getPendingUsers().remove(passenger);
                ride.getRegisteredUsers().add(passenger);
                int seats = ride.getPost().getAvailableSeatsLeft();
                log.debug("{} seats left on this ride", seats);
                if (seats > 0) { // if there is available seats on this ride
                    ride.getPost().setAvailableSeatsLeft(seats - 1);
                    rideRepo.save(ride);
                    return true;
                } else {
                    return false;
                }

            } else {
                log.warn(MessageConstants.NOT_PENDING_ON_GIVEN_RIDE);
                throw new PassengerNotPendingOnGivenRideException(MessageConstants.NOT_PENDING_ON_GIVEN_RIDE);
            }

        } else {
            log.warn(MessageConstants.RIDE_AND_DRIVER_MISMATCH);
            throw new RideNotMatchDriverException(MessageConstants.RIDE_AND_DRIVER_MISMATCH);
        }

    }


    @Override
    public void rejectPassengerOnPendingRide(Long passengerId, Long rideId) {  //for drivers
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                });
        User passenger = userRepo.findById(passengerId)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.PASSENGER_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.PASSENGER_ID_NOT_FOUND);
                });
        Ride ride = rideRepo.findById(rideId)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
                });
        if (ride.getPost().getDriver().equals(driver)) {
            log.debug("user that are pending on this ride {}", ride.getPendingUsers());
            if (ride.getPendingUsers().contains(passenger)) {
                ride.getPendingUsers().remove(passenger);
                rideRepo.save(ride);
            } else {
                log.warn(MessageConstants.NOT_PENDING_ON_GIVEN_RIDE);
                throw new PassengerNotPendingOnGivenRideException(MessageConstants.NOT_PENDING_ON_GIVEN_RIDE);
            }

        } else {
            log.warn(MessageConstants.RIDE_AND_DRIVER_MISMATCH);
            throw new RideNotMatchDriverException(MessageConstants.RIDE_AND_DRIVER_MISMATCH);
        }

    }


    @Override
    public List<PassengerRideDto> historyOfPastRidesForPassenger(int page) {  //for passenger
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        User passenger = userRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.INCORRECT_CREDENTIALS);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.INCORRECT_CREDENTIALS);
                });

        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        return passenger.getRegistered().stream()
                .filter(Ride::isFinished).map(ride -> {

                            Converter<Collection<User>, Integer> toSizeConverter = val -> val.getSource().size();
                            return modelMapper.typeMap(Ride.class, PassengerRideDto.class)
                                    .addMapping(ride1 -> ride1.getRoute().getContainsAddresses(), PassengerRideDto::setRoute)
                                    .addMapping(r -> r.getPost().getDriver(), PassengerRideDto::setDriver)
                                    .addMappings(mapper -> mapper.using(toSizeConverter)
                                            .map(Ride::getRegisteredUsers, PassengerRideDto::setTotalNumberOfRegisteredPassengers))
                                    .map(ride);
                        }
                )
                .skip(start).limit(size).toList();
    }


    @Override
    public List<PassengerRideDto> upcomingRidesForPassenger(int page) {  //for passenger
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        User passenger = userRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.INCORRECT_CREDENTIALS);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.INCORRECT_CREDENTIALS);
                });

        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        return passenger.getRegistered().stream()
                .filter(ride -> !ride.isFinished())
                .map(ride -> {
                    Converter<Collection<User>, Integer> toSizeConverter = val -> val.getSource().size();
                    return modelMapper.typeMap(Ride.class, PassengerRideDto.class)
                            .addMapping(ride1 -> ride1.getRoute().getContainsAddresses(), PassengerRideDto::setRoute)
                            .addMapping(r -> r.getPost().getDriver(), PassengerRideDto::setDriver)
                            .addMappings(mapper -> mapper.using(toSizeConverter)
                                    .map(Ride::getRegisteredUsers, PassengerRideDto::setTotalNumberOfRegisteredPassengers))
                            .map(ride);
                })
                .skip(start).limit(size).toList();
    }


    @Override
    public List<DriverRideDto> historyOfPastRidesForDriver(int page) {   //for driver
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                });
        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        List<RoutePost> posts = routePostRepo.findByDriver(driver);
        log.debug("all posts of this driver {}", posts);
        return posts.stream().map(RoutePost::getRide)
                .filter(Ride::isFinished)
                .map(ride ->
                        modelMapper.typeMap(Ride.class, DriverRideDto.class)
                                .addMapping(r -> r.getRoute().getContainsAddresses(), DriverRideDto::setRoute)
                                .map(ride))
                .skip(start)
                .limit(size)
                .toList();

    }


    @Override
    public List<DriverRideDto> upcomingRidesForDriver(int page) {  //for driver
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        Driver driver = driverRepo.findByUserName(username)
                .orElseThrow(() -> {
                    log.warn(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                    throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.DRIVER_NOT_AUTHENTICATED);
                });
        int size = Constants.PAGE_SIZE;
        int start = page > 1 ? page * size : 0;
        List<RoutePost> posts = routePostRepo.findByDriver(driver);
        return posts.stream().map(RoutePost::getRide)
                .filter(ride -> !ride.isFinished())
                .map(ride -> modelMapper.typeMap(Ride.class, DriverRideDto.class)
                        .addMapping(r -> r.getRoute().getContainsAddresses(), DriverRideDto::setRoute)
                        .map(ride))
                .skip(start)
                .limit(size)
                .toList();
    }

    @Override
    public void cancelRideByDriver(long rideId) {
        Ride ride = rideRepo.findById(rideId).orElseThrow(() -> {
            log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
            throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
        });
        if (!ride.isFinished()) {
            rideRepo.delete(ride);
        } else {
            log.warn(MessageConstants.CANCEL_FINISHED_RIDE);
            throw new FinishedRideException(MessageConstants.CANCEL_FINISHED_RIDE);
        }


    }

    @Override
    public void unregisterFromRideByPassenger(long rideId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails principal = (UserDetails) authentication.getPrincipal();
        String username = principal.getUsername();
        User passenger = userRepo.findByUserName(username).orElseThrow(() -> {
            log.warn(MessageConstants.INCORRECT_CREDENTIALS);
            throw new AuthenticationExceptionForDriverOrPassenger(MessageConstants.INCORRECT_CREDENTIALS);
        });
        Ride ride = rideRepo.findById(rideId).orElseThrow(() -> {
                    log.warn(MessageConstants.RIDE_ID_NOT_FOUND);
                    throw new IdNotFoundException(MessageConstants.RIDE_ID_NOT_FOUND);
                }

        );
        if (!ride.isFinished()) {
            ride.getRegisteredUsers().remove(passenger);
            rideRepo.save(ride);
        } else {
            log.warn(MessageConstants.UNREGISTER_FROM_FINISHED_RIDE);
            throw new FinishedRideException(MessageConstants.UNREGISTER_FROM_FINISHED_RIDE);
        }


    }
}
