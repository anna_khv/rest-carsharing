package com.carsharing.capstone.service;

import com.carsharing.capstone.dto.CreatePostDto;
import com.carsharing.capstone.model.Ride;

public interface IPostService {
    void  bookARide(Long id);

    void  followRoute(Long id);

    Long createRoutePost(CreatePostDto routePost);

    int calculateRideCost(Ride ride);

    void deletePost(long postId);
}
