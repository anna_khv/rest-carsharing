package com.carsharing.capstone.config;

import com.carsharing.capstone.validator.ValidatePasswordResetToken;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();

    }
    @Bean
    public ValidatePasswordResetToken validatePasswordResetToken() {
        return new ValidatePasswordResetToken();
    }

}
