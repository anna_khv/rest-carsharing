package com.carsharing.capstone.config;

import com.carsharing.capstone.security.CustomAuthFilter;
import com.carsharing.capstone.security.CustomFailureHandler;
import com.carsharing.capstone.security.CustomLogoutHandler;
import com.carsharing.capstone.security.CustomSuccessHandler;
import com.carsharing.capstone.util.Constants;
import io.swagger.models.HttpMethod;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserDetailsService myUserDetailService;


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder manager) {
        log.debug("authenticate user");
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(myUserDetailService);
        provider.setPasswordEncoder(new BCryptPasswordEncoder());
        manager.authenticationProvider(provider);
    }


    @Override
    public void configure(WebSecurity web) {   //h2 and swagger doesnt work without this

        web
                .ignoring()
                .antMatchers("/h2-console/**")
                .antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**");


    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf()
                .disable()
                .formLogin()
                .loginPage("/authorisation/login")
                .successHandler(new CustomSuccessHandler())
                .failureHandler(new CustomFailureHandler())
                .and()
                .addFilterBefore(new CustomAuthFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers("/authorisation/**").permitAll()
                .antMatchers("/home/**").permitAll()
                .antMatchers("/post/passenger/**").hasRole(Constants.PASSENGER)
                .antMatchers("/post/driver/**").hasRole(Constants.DRIVER)
                .antMatchers("/profile/**").permitAll()
                .antMatchers("/h2-console/**").permitAll()//need to see what is in database
                .antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/swagger*/**").permitAll()
                .antMatchers("/ride/passenger/**").hasRole(Constants.PASSENGER)
                .antMatchers("/ride/driver/**").hasRole(Constants.DRIVER)
                .antMatchers("/ride/review/**").hasAnyRole(Constants.PASSENGER,Constants.DRIVER)
                .antMatchers("/admin/**").hasRole(Constants.ADMIN)
                .anyRequest().hasRole("PASSENGER")
                .and()
                .logout().logoutUrl("/authorisation/logout").permitAll().invalidateHttpSession(true)
                .clearAuthentication(true)
                .logoutSuccessHandler(new CustomLogoutHandler());


    }
}
