package com.carsharing.capstone.converter;

import com.carsharing.capstone.model.Rate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Arrays;

@Converter
public class RateConverter implements AttributeConverter<Rate, Long> {

    @Override
    public Long convertToDatabaseColumn(Rate rate) {
        return rate.getValue();
    }

    @Override
    public Rate convertToEntityAttribute(Long dbData) {
        return Arrays.stream(Rate.values()).filter(val -> val.getValue() == dbData).findFirst().orElseThrow(() -> {
                    throw new IllegalArgumentException("wrong input data");

                }

        );
    }


}
