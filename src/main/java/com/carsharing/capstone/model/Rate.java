package com.carsharing.capstone.model;

public enum Rate {

    ONE(1), TWO(2), THREE(3), FOUR(4), FIVE(5);
    private final long value;

    Rate(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }




}
