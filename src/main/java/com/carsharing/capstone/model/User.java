package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIdentityInfo(scope = User.class,
        generator = ObjectIdGenerators.PropertyGenerator.class, property = "idNumber"
)
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idNumber;

    @Column(name = "userName", unique = true )  //have to ask about this
    private String userName;

    @Column(name = "fName")
    private String firstName;

    @Column(name = "lName")
    private String lastName;

    @Column(name="email", unique=true)
    private String email;

    @Column(name = "phoneNumber")
    private String phoneNumber;

    @Column(name = "isDriver")
    private boolean isDriver;

    @Column(name = "password") // password should be encrypted
    private String password;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "userFollowRoutes", joinColumns = @JoinColumn(name = "userId"), inverseJoinColumns = @JoinColumn(name = "routeId"))
    private List<Route> followsRoutes;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL)
    @JsonManagedReference(value = "User-Rev")
    private List<Review> reviewsOfUser;

    @OneToMany(mappedBy = "addresseeUser",cascade = CascadeType.ALL)
    @JsonManagedReference(value = "User-Rev1")
    private List<Review> reviewsToUser;

    @OneToOne(mappedBy="user",cascade = CascadeType.ALL)
    private PasswordResetToken token;

    @ManyToMany(mappedBy = "registeredUsers")
    private List<Ride> registered;


    @ManyToMany(mappedBy = "pendingUsers")
    private List<Ride> pending;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="user_roles",
    joinColumns = @JoinColumn(
            name="userId", referencedColumnName = "idNumber"),
    inverseJoinColumns = @JoinColumn(
            name="roleId", referencedColumnName = "roleId"
    ))
    private List<Role> roles;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return idNumber == user.idNumber && isDriver == user.isDriver && Objects.equals(userName, user.userName) && Objects.equals(firstName, user.firstName) && Objects.equals(lastName, user.lastName) && Objects.equals(phoneNumber, user.phoneNumber) && Objects.equals(password, user.password) && Objects.equals(followsRoutes, user.followsRoutes) && Objects.equals(reviewsOfUser, user.reviewsOfUser) && Objects.equals(reviewsToUser, user.reviewsToUser) && Objects.equals(registered, user.registered) && Objects.equals(pending, user.pending);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNumber, userName, firstName, lastName, phoneNumber, isDriver, password, followsRoutes, reviewsOfUser, reviewsToUser, registered, pending);
    }

    @Override
    public String toString() {
        return "User{" +
                "idNumber=" + idNumber +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", isDriver=" + isDriver +
                ", password='" + password + '\'' +
                ", followsRoutes=" + followsRoutes +
                ", reviewsofUser=" + reviewsOfUser +
                ", reviewstoUser=" + reviewsToUser +
                ", registered=" + registered +
                ", pending=" + pending +
                '}';
    }
}
