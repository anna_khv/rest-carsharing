package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Address")
@Setter
@Getter
//@EqualsAndHashCode(exclude={"inroutes"})
//@ToString(exclude={"inroutes"})
@JsonIdentityInfo(scope=Address.class,
        generator = ObjectIdGenerators.PropertyGenerator.class, property="id"
)
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
//	@Column(name="localPosition")
    private LocationPosition position;

    @Column(name = "adStreet")
    private String adStreet;

    @Column(name="adNumber")
    private int streetNumber;

    @ManyToMany(mappedBy = "containsAddresses")
    @JsonBackReference()
    private List<Route> inroutes;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return id == address.id && streetNumber == address.streetNumber && position == address.position && Objects.equals(adStreet, address.adStreet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position, adStreet, streetNumber);
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", position=" + position +
                ", adStreet='" + adStreet + '\'' +
                ", streetNumber=" + streetNumber +
                '}';
    }
}
