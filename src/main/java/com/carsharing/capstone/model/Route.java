package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Getter
@Setter
//@EqualsAndHashCode(exclude = {"inRides","routeIsFollowedBy"})
//@ToString(exclude = {"inRides","routeIsFollowedBy"})
@Table(name = "Route")
@JsonIdentityInfo(scope=Route.class,
        generator = ObjectIdGenerators.PropertyGenerator.class, property="id"
)
public class Route {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "RouteAddress", joinColumns = @JoinColumn(name = "RouteId"), inverseJoinColumns = @JoinColumn(name = "AddressId"))
    private List<Address> containsAddresses;

    @OneToMany(mappedBy = "route", cascade = CascadeType.ALL)
    @JsonBackReference(value="Ride-Route")
    private List<Ride> inRides;

    @ManyToMany(mappedBy = "followsRoutes")
    @JsonBackReference()
    private List<User> routeIsFollowedBy;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Route route = (Route) o;
        return id == route.id && Objects.equals(containsAddresses, route.containsAddresses);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, containsAddresses);
    }

    @Override
    public String toString() {
        return "Route{" +
                "id=" + id +
                ", containsAddresses=" + containsAddresses +
                '}';
    }
}
