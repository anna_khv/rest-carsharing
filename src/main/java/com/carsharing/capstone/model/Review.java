package com.carsharing.capstone.model;

import com.carsharing.capstone.converter.RateConverter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Setter
@Getter
//@EqualsAndHashCode(exclude ={"ride", "user","addresseeUser"} )
//@ToString(exclude ={"ride", "user","addresseeUser"} )
public class Review {
    public static final int EXPIRATION = 60 * 5;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long reviewId;

    @Column(name = "text")
    private String text;

    @Convert(converter = RateConverter.class)
    private Rate rate;


    @ManyToOne()
    @JsonBackReference(value = "Ride-Review")
    @JoinColumn(name = "rideId", referencedColumnName = "rideId")
    private Ride ride;

    @ManyToOne()
    @JsonBackReference(value = "User-Rev")
    @JoinColumn(name = "authorId", referencedColumnName = "idNumber")
    private User user;

    @ManyToOne()
    @JsonBackReference(value = "User-Rev1")
    @JoinColumn(name = "addresseeId", referencedColumnName = "idNumber")
    private User addresseeUser;

    @Column(name = "expiryTime")
    private LocalDateTime expiryTime;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Review review = (Review) o;
        return reviewId == review.reviewId && Objects.equals(text, review.text) && rate == review.rate;
    }


    @Override
    public int hashCode() {
        return Objects.hash(reviewId, text, rate);
    }

    @Override
    public String toString() {
        return "Review{" +
                "reviewId=" + reviewId +
                ", text='" + text + '\'' +
                ", rate=" + rate +
                '}';
    }
}
