package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "RoutePost")
@JsonIdentityInfo(scope = RoutePost.class,
        generator = ObjectIdGenerators.PropertyGenerator.class, property = "routePostId"
)
@Setter
@Getter
//@EqualsAndHashCode(exclude = {"driver"})
//@ToString(exclude = {"driver"})
public class RoutePost {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long routePostId;
    @Column(name = "rideDescriprion")
    private String rideDescription;
    @Column(name = "carDescription")
    private String carDescription;

    @Column(name = "seatsLeft")
    private int availableSeatsLeft; // this field is a derived field so it can be omitted

    @OneToOne(cascade=CascadeType.ALL)
    @JsonManagedReference(value = "RoutePost-Ride")
    @JoinColumn(name = "rId", referencedColumnName = "rideId",nullable = false)
    private Ride ride;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference(value = "Driver-RoutePost")
    @JoinColumn(name = "driverId", referencedColumnName = "userId")
    private Driver driver;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoutePost routePost = (RoutePost) o;
        return routePostId == routePost.routePostId && availableSeatsLeft == routePost.availableSeatsLeft && Objects.equals(rideDescription, routePost.rideDescription) && Objects.equals(carDescription, routePost.carDescription) && Objects.equals(ride, routePost.ride);
    }

    @Override
    public int hashCode() {
        return Objects.hash(routePostId, rideDescription, carDescription, availableSeatsLeft, ride);
    }

    @Override
    public String toString() {
        return "RoutePost{" +
                "routePostId=" + routePostId +
                ", rideDescription='" + rideDescription + '\'' +
                ", carDescription='" + carDescription + '\'' +
                ", availableSeatsLeft=" + availableSeatsLeft +
                ", ride=" + ride +
                '}';
    }
}
