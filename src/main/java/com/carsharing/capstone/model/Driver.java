package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Setter
@Getter
@PrimaryKeyJoinColumn(name = "userId")
public class Driver extends User {


    @Column(name = "carRegNum")
    private String carRegistrationNumber;
    @Column(name = "driverLicense")
    private String driverLicenseNumber;

    @OneToMany(mappedBy = "driver", cascade = CascadeType.ALL)
    @JsonManagedReference(value="Driver-RoutePost")
    private List<RoutePost> driveRoutePosts;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Driver driver = (Driver) o;
        return Objects.equals(carRegistrationNumber, driver.carRegistrationNumber) && Objects.equals(driverLicenseNumber, driver.driverLicenseNumber) && Objects.equals(driveRoutePosts, driver.driveRoutePosts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), carRegistrationNumber, driverLicenseNumber, driveRoutePosts);
    }

    @Override
    public String toString() {
        return "Driver{" +
                "carRegistrationNumber='" + carRegistrationNumber + '\'' +
                ", driverLicenseNumber='" + driverLicenseNumber + '\'' +
                ", driveRoutePosts=" + driveRoutePosts +
                '}';
    }
}
