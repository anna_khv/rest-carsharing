package com.carsharing.capstone.model;


public enum LocationPosition {
    START_POINT, END_POINT, PASSING_POINT

}
