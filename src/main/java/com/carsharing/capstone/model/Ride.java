package com.carsharing.capstone.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "Ride")
@JsonIdentityInfo(scope = RoutePost.class,
        generator = ObjectIdGenerators.PropertyGenerator.class, property = "rideId"
)
@Getter
@Setter
public class Ride {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long rideId;

    @Column(name = "date")
    private Date date;

    @Column(name = "total")
    private int totalNumberOfPassengers;

    @Column(name = "finished")
    private boolean finished;

    @ManyToOne(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    // @JsonManagedReference(value="Ride-Route") ??
    @JoinColumn(name = "routeId", referencedColumnName = "id") // this is id of route
    private Route route;

    @OneToOne(mappedBy = "ride", cascade = CascadeType.ALL)
    @JsonBackReference(value = "RoutePost-Ride")
    private RoutePost post;

    @ManyToMany
    @JoinTable(name = "usersOnRide", joinColumns = @JoinColumn(name = "rideId"), inverseJoinColumns = @JoinColumn(name = "userId"))
    private List<User> registeredUsers;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(name = "pendingUsersOnRide", joinColumns = @JoinColumn(name = "rideId"), inverseJoinColumns = @JoinColumn(name = "userId"))
    private List<User> pendingUsers;

    @OneToMany(mappedBy = "ride", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "Ride-Review")
    private List<Review> reviews;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ride ride = (Ride) o;
        return rideId == ride.rideId && totalNumberOfPassengers == ride.totalNumberOfPassengers && finished == ride.finished && Objects.equals(date, ride.date) && Objects.equals(route, ride.route) && Objects.equals(reviews, ride.reviews);
    }

    @Override
    public int hashCode() {
        return Objects.hash(rideId, date, totalNumberOfPassengers, finished, route, reviews);
    }

    @Override
    public String toString() {
        return "Ride{" +
                "rideId=" + rideId +
                ", date=" + date +
                ", totalNumberOfPassengers=" + totalNumberOfPassengers +
                ", finished=" + finished +
                ", route=" + route +
                ", reviews=" + reviews +
                '}';
    }
}
