package com.carsharing.capstone.exception;

public class IdNotFoundException extends RuntimeException{


    public IdNotFoundException(String message){
        super(message);

    }
}
