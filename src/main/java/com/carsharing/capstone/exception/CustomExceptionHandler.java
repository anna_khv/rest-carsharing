package com.carsharing.capstone.exception;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class CustomExceptionHandler {


    @ExceptionHandler(value = IdNotFoundException.class)
    public ResponseEntity<String> handleIdNotFoundException(IdNotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = NullPointerException.class)
    public ResponseEntity<String> handleNullPointerException(NullPointerException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = RideNotMatchDriverException.class)
    public ResponseEntity<String> handleRideNotMatchDriverException(RideNotMatchDriverException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    @ResponseBody
    public ResponseEntity<String> handleMethodArgumentNoValid(MethodArgumentNotValidException ex) {
     //   Map<String, String> errors = new HashMap<>();
        JSONObject obj=new JSONObject();
        HttpStatus status = HttpStatus.BAD_REQUEST;
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String message = error.getDefaultMessage();
            String fieldName = ((FieldError) error).getField();
            try {
                obj.put(fieldName, message);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
        return new ResponseEntity<>(obj.toString(),status);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<String> handleConstraintViolation(DataIntegrityViolationException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = PassengerNotPendingOnGivenRideException.class)
    public ResponseEntity<String> handlePassengerNotPendingOnRideException(PassengerNotPendingOnGivenRideException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = DriverNotCreatedException.class)
    public ResponseEntity<String> handleDriverNotCreatedException(DriverNotCreatedException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = UserNotOnGivenRideException.class)
    public ResponseEntity<String> handleUserNotOnRide(UserNotOnGivenRideException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = ReviewAlreadyPresentForThisRideAndAddresseeException.class)
    public ResponseEntity<String> reviewNotFound(ReviewAlreadyPresentForThisRideAndAddresseeException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }
    @ExceptionHandler(value = ReviewNotByGivenUserException.class)
    public ResponseEntity<String> reviewNotOfGivenUser(ReviewNotByGivenUserException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }
    @ExceptionHandler(value = FinishedRideException.class)
    public ResponseEntity<String> FinishedRide(FinishedRideException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }
    @ExceptionHandler(value = ReviewUpdateExpiredException.class)
    public ResponseEntity<String> reviwUpdateExpired(ReviewUpdateExpiredException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }
    @ExceptionHandler(value = AuthenticationExceptionForDriverOrPassenger.class)
    public ResponseEntity<String> handleAuthenticationExceptionForDriverOrPassenger(AuthenticationExceptionForDriverOrPassenger ex) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = TokenNotFoundException.class)
    public ResponseEntity<String> handleTokenNotFoundException(TokenNotFoundException ex) {
        HttpStatus status = HttpStatus.NOT_IMPLEMENTED;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    @ExceptionHandler(value = TokenExpiredException.class)
    public ResponseEntity<String> handletokenExpiredException(TokenExpiredException ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        return new ResponseEntity<>(prepareErrorJson(status, ex), status);
    }

    private String prepareErrorJson(HttpStatus status, Exception ex) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status.value());
            jsonObject.put("error", status.getReasonPhrase());
            jsonObject.put("message", ex.getMessage());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }
}
