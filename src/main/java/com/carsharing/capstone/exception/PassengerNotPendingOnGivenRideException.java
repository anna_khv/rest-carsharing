package com.carsharing.capstone.exception;

public class PassengerNotPendingOnGivenRideException extends RuntimeException{

    public PassengerNotPendingOnGivenRideException(String message){
        super(message);
    }
}
