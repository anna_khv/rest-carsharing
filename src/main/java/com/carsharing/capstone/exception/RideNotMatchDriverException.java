package com.carsharing.capstone.exception;

public class RideNotMatchDriverException extends RuntimeException{

    public RideNotMatchDriverException(String message){
        super(message);
    }
}
