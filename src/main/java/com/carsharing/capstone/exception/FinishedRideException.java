package com.carsharing.capstone.exception;

public class FinishedRideException extends RuntimeException{
   public FinishedRideException(String ex){
        super(ex);
    }
}
