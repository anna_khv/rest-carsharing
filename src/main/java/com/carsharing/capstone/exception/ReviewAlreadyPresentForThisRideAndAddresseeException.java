package com.carsharing.capstone.exception;

public class ReviewAlreadyPresentForThisRideAndAddresseeException extends RuntimeException{
    public ReviewAlreadyPresentForThisRideAndAddresseeException(String ex){
        super(ex);
    }
}
