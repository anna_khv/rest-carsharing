package com.carsharing.capstone.exception;

public class ReviewNotByGivenUserException extends RuntimeException{
    public ReviewNotByGivenUserException(String ex){
        super(ex);
    }
}
