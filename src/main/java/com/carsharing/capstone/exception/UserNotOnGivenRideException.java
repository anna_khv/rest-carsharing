package com.carsharing.capstone.exception;

public class UserNotOnGivenRideException extends RuntimeException{

   public UserNotOnGivenRideException(String message){
       super(message);

    }
}
