package com.carsharing.capstone.exception;

import org.springframework.security.core.AuthenticationException;

public class AuthenticationExceptionForDriverOrPassenger extends AuthenticationException {

    public AuthenticationExceptionForDriverOrPassenger(String massage){
        super(massage);
    }

}
