package com.carsharing.capstone.exception;

public class DriverNotCreatedException extends RuntimeException {

    public DriverNotCreatedException(String message){
        super(message);
    }
}
