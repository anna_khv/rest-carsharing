package com.carsharing.capstone.exception;

public class ReviewUpdateExpiredException extends RuntimeException{
    public ReviewUpdateExpiredException(String ex){
        super(ex);
    }
}
