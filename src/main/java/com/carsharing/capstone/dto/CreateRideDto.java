package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;
import java.sql.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateRideDto {

    private Date date;
    @Positive
    private int totalNumberOfPassengers;
    @NotEmpty
    private List<CreateAddressDto> route;

}
