package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostDto {

    private long routePostId;
    private String rideDescription;
    private String carDescription;
    private int availableSeatsLeft; // this field is a derived field so it can be omitted
    private RideDto ride;
    private DriverProfileDto driverProfile;

}
