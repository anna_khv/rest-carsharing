package com.carsharing.capstone.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;
@Data
@NoArgsConstructor
public class PassengerRideDto {

    private long rideId;
    private Date date;
    private int totalNumberOfRegisteredPassengers;
    private List<AddressDto> route;
    private UserDto driver;


}
