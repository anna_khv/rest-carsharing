package com.carsharing.capstone.dto;

import com.carsharing.capstone.model.Rate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReviewDto {

    private long reviewId;
    private String text;
    private Rate rate;
    private UserDto author;
    private UserDto addressee;
    private long rideId;

}
