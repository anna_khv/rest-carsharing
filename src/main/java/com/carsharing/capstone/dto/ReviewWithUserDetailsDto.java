package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReviewWithUserDetailsDto {
    private ReviewDto review;
    private String name;
    private String surname;
    private long userId;
    private long rideId;
}
