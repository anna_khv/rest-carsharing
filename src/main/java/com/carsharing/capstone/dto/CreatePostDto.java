package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreatePostDto {
    @NotBlank
    private String rideDescription;
    @NotBlank
    private String carDescription;
    @NotNull
    private CreateRideDto ride;
}
