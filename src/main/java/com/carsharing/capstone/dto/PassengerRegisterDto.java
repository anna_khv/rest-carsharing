package com.carsharing.capstone.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class PassengerRegisterDto {

    @NotBlank(message = "username should not be empty")
    @Pattern(regexp ="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{6,}$", message ="username does not meet requirements" )
    private   String userName;
    @NotBlank(message = "email should not be empty")
    @Pattern(regexp = "^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message ="email does not meet requirements" )
    private   String email;
    @NotBlank(message = "firstname should not be empty")
    private String firstName;
    @NotBlank(message = "lastname should not be empty")
    private  String lastName;
    @NotBlank(message = "phone number should not be empty")
    private  String phoneNumber;
    @NotBlank(message = "password should not be empty")
    @Pattern(regexp ="^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$",message ="password does not meet requirements" )
    private  String password;

}
