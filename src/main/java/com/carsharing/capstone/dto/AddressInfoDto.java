package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressInfoDto {
    @Valid
    AddressData addressAtStartLocation;
    List<AddressData> addressAtPassingLocation;
    @Valid
    AddressData addressAtEndLocation;

    /*
    private  String startaddressStreetName;
    private int startaddressStreetNumber;
    private String passingAddressStreetName;
    private int passingAddressStreetNumber;
    private String endAddressStreetName;
    private int endAddressStreetNumber;
*/

}
