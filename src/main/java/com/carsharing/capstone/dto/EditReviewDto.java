package com.carsharing.capstone.dto;

import com.carsharing.capstone.model.Rate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EditReviewDto {
    private long reviewId;
    private String text;
    private Rate rate;
}
