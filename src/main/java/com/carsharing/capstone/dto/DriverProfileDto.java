package com.carsharing.capstone.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
public class DriverProfileDto {
    private long idNumber;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private long totalNumberOfRides;
    private long totalNumberOfReviews;
    private double avgRating;


}
