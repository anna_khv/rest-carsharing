package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DriverDto {
    @NotBlank(message = "carRegistrationNumber should be provided")
    private String carRegistrationNumber;
    @NotBlank(message = "driverLicenseNumber should be provided")
    private String driverLicenseNumber;
}
