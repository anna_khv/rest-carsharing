package com.carsharing.capstone.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.util.List;
@Data
@NoArgsConstructor
public class DriverRideDto {

    private long rideId;
    private Date date;
    private List<AddressDto> route;
    private List<UserDto> registeredUsers;
}
