package com.carsharing.capstone.dto;

import com.carsharing.capstone.model.LocationPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateAddressDto {
    private LocationPosition position;
    private String AdStreet;
    private int streetNumber;

}
