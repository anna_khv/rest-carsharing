package com.carsharing.capstone.dto;

import com.carsharing.capstone.model.LocationPosition;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AddressDto {
    private long id;
    private LocationPosition position;
    private String adStreet;
    private int streetNumber;
}
