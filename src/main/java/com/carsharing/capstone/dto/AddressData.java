package com.carsharing.capstone.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddressData {
    @NotBlank(message = "start end end address should be indicated")
    private String Street;
    private int StreetNumber;
}
